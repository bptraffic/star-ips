/**
 * This file is part of:
 *    star-ips -- a family of incremental GMRF estimation algorithms.
 *
 * Authors:
 *   Cyril Furtlehner, cyril.furtlehner@inria.fr
 *   Victorin Martin, victorin.martin@gmail.com
 *   Jean-Marc Lasgouttes, jean-marc.lasgouttes@inria.fr
 *
 * This software is distributed under the Creative Commons License
 * BY-NC-SA. The complete license can be found here
 *   http://creativecommons.org/licenses/by-nc-sa/4.0/legalcode
 */

#include "GMRF.h"
#include "Parameters.h"

extern Parameters p;

using namespace std;
using namespace Eigen;

GMRF::GMRF(MatrixXd const & PMatrix)
{
	// actually Nn == p.Nv
	Nn = PMatrix.rows();
	build_neighborhood(PMatrix);
}

void
GMRF::re_index(int n, int j, int k)
{
	int const n1 = Vois[n][j];
	int const n2 = Vois[n][k];
	int const i1 = ind[n][j];
	int const i2 = ind[n][k];
	Vois[n][j] = n2;
	Vois[n][k] = n1;
	ind[n1][i1] = k;
	ind[n2][i2] = j;
	ind[n][j] = i2;
	ind[n][k] = i1;
}

// Pruning everything
// FIXME: it looks like this just resets everything. There has to be a
// better way to do that. I keep it like that for the benefit of working code.
void GMRF::prune_all()
{
	for (int n = 0; n < Nn; n++) {
		for (int j = 0; j < nv[n]; j++) {
			re_index(n, j, nv[n] - 1);
			nv[n]--;
			j--;
		}
	}
	int const Narc = std::accumulate(nv.begin(), nv.end(), 0);
	Km = 1.0 * Narc / Nn;
}

void GMRF::prune_from_Pmatrix (MatrixXd const & A)
{
	for (int n = 0; n < Nn; n++) {
		for (int j = 0; j < nv[n]; j++) {
			if (A(n,Vois[n][j]) == 0) {	//Edge not part of the graph
				re_index(n, j, nv[n] - 1);
				nv[n]--;
				j--;
			}
		}
	}
	int const Narc = std::accumulate(nv.begin(), nv.end(), 0);
	Km = 1.0 * Narc / Nn;
}

bool GMRF::build_neighborhood(MatrixXd const & A)
{
	Vois.resize(Nn, Nn);
	ind.resize(Nn, Nn);
	nv.resize(Nn);
	nv0.resize(Nn);
	int ne = 0;
	for (int n1 = 0; n1 < Nn; n1++) {
		for (int n2 = n1 + 1; n2 < Nn; n2++) {
			if (abs(A(n1,n2)) > POSITIVE_TH) {
				Vois[n1][nv[n1]] = n2;
				ind[n1][nv[n1]] = nv[n2];
				Vois[n2][nv[n2]] = n1;
				ind[n2][nv[n2]] = nv[n1];
				nv[n1]++;
				nv[n2]++;
				ne++;
			}
		}
	}
	for (int i = 0; i < Nn; i++) {
		nv0[i] = nv[i];
	}
	if (p.VERBOSITY > 1) {
		cout << "-- GMRF Model created -- Number of variables: " << Nn
			 << " -- Mean connectivity: " << 2.0 * ne / Nn
			 << " -- " << endl;
	}
	return true;
}

void GMRF::set_XX_YY_Mask(int NvX,int NvY)         //blacklist all inter links between X and Y
{
	cout << "Set XX-YY mask\n"; 
	int N = NvX+NvY;
	for (int n1=0;n1<N;n1++)
		nv0[n1] = N-1;
	for (int n1=0;n1<NvX;n1++)
	{
		for (int j=0;j<nv0[n1];j++)
		{
			int n2 = Vois[n1][j];
			int ii = ind[n1][j];
			if (n2 >= NvX)
			{
				re_index(n1, j, nv0[n1] - 1);
				nv0[n1]--;
				re_index(n2, ii, nv0[n2] - 1);
				nv0[n2]--;
				j--;
			}
		}
	}
}

void GMRF::set_XY_Mask(int NvX,int NvY)           //blacklist all intern links in X and Y
{
	cout << "Set XY mask\n"; 
	int N = NvX+NvY;
	for (int n1=0;n1<N;n1++)
		nv0[n1] = N-1;
	for (int n1=0;n1<N;n1++)
	{
		for (int j=nv[n1];j<nv0[n1];j++)
		{
			int n2 = Vois[n1][j];
			int ii = ind[n1][j];
			if ((n1< NvX && n2 < NvX) || (n1 >= NvX && n2 >= NvX) )
			{
				re_index(n1, j, nv0[n1] - 1);
				nv0[n1]--;
				re_index(n2, ii, nv0[n2] - 1);
				nv0[n2]--;
				j--;
			}
		}
	}
}
