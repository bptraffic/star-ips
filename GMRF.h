/** -*- c++ -*-
 * This file is part of:
 *    star-ips -- a family of incremental GMRF estimation algorithms.
 *
 * Authors:
 *   Cyril Furtlehner, cyril.furtlehner@inria.fr
 *   Victorin Martin, victorin.martin@gmail.com
 *   Jean-Marc Lasgouttes, jean-marc.lasgouttes@inria.fr
 *
 * This software is distributed under the Creative Commons License
 * BY-NC-SA. The complete license can be found here
 *   http://creativecommons.org/licenses/by-nc-sa/4.0/legalcode
 */

#ifndef GMRF_H
#define GMRF_H

#include "Tools.h"

class GMRF
{

public:
	GMRF (Eigen::MatrixXd const &);

	// Matrix size (number of variables)
	int Nn;
	// connectiviry
	double Km;
	// Vois[i][k] is the indice of the k-th neighbor of i
	// ind is ???
	vector2<int> Vois, ind;
	// nv[i] is the number of neighbors of i
	// nv0[i] is nv[i] plus the number of potential new neighbors of i
	std::vector<int> nv, nv0;

	void prune_from_Pmatrix(Eigen::MatrixXd const &);
	void prune_all();
	void re_index(int, int, int);
	void set_XX_YY_Mask(int, int);
	void set_XY_Mask(int, int);
private:
	bool build_neighborhood(Eigen::MatrixXd const &);

};

#endif //GMRF_H
