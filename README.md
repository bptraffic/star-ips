# ★-IPS – a family of incremental GMRF estimation algorithms.

See file NEWS for recent changes.

Authors:
 - Cyril Furtlehner, <cyril.furtlehner@inria.fr>
 - Victorin Martin, <victorin.martin@gmail.com>
 - Jean-Marc Lasgouttes, <jean-marc.lasgouttes@inria.fr>




About star-ips
--------------

star-ips is an open-source program that implements the family of
algorithms ★-IPS, based on the Iterative Proportional Scaling (IPS)
procedure. Using IPS, it incrementally selects links and
partial correlations of a GMRF, imposing spectral and/or topological
constraints. For more details about the ★-IPS framework, see the paper

Martin V., Furtlehner C., Han Y., and Lasgouttes J.-M., [*GMRF
Estimation under Topological and Spectral
constraints*](ECML14_IPS.pdf),
presented at [ECML/PKDD 2014](http://www.ecmlpkdd2014.org/), Nancy,
France.

**Abstract**: We investigate the problem of Gaussian Markov random
field selection under a non-analytic constraint: the estimated models
must be compatible with a fast inference algorithm, namely the
Gaussian belief propagation algorithm. To address this question, we
introduce the ★-IPS framework, based on iterative proportional
scaling, which incrementally selects candidate links in a greedy
manner. Besides its intrinsic sparsity-inducing ability, this
algorithm is flexible enough to incorporate various spectral
constraints, like e.g. walk summability, and topological constraints,
like avoiding the formation of short loops. Experimental tests on
various datasets, including traffic data from San Fransisco Bay area,
indicate that this approach can deliver, with reasonable computational
cost, a broad range of efficient inference models, which are not
accessible through penalization with traditional sparsity-inducing
norms.


Building star-ips
-----------------

Build star-ips requires a C++ compiler that handles C++11. The
Makefile assumes gcc, but clang can be used too.

To build star-ips, just execute

    make

Note that it could be necessary to modify the Makefile to cope with
your particular system. It has been successfully tested under
various Ubuntu distributions.

star-ips has not been tested under Windows, but since it is written in
standard C++, it should work.


Using star-ips
--------------

A complete list of the parameters with their meaning is displayed
when executing

    ./star-ips -h

Just execute `./star-ips` with your parameters.
Mandatory parameters are the covariance matrix (given by a simple
text file) and the number of variables. Usage is

    ./star-ips -nv 1000 -cov_file cov.dat [-otherparam value]...

Parameters can alternatively be read from a configuration file using

    ./star-ips -config_file name.

Parameters given in command line have priority over the configuration
file. An example configuration file is given (`ex.config`).


Examples
--------

_[coming soon]_

Legal issues
------------

This software is distributed under the Creative Commons License
BY-NC-SA. The complete license can be found here:

  <http://creativecommons.org/licenses/by-nc-sa/4.0/legalcode>

Basically you're free to use, modify and distribute it as long as
you acknowledge us, use the same license and do not sell it.

star-ips is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY.
