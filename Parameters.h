// -*- C++ -*-
/**
	This is Parameters version 2.0 -- 12/06/2019.
	Author: Jean-Marc Lasgouttes, <jean-marc.lasgouttes@inria.fr>
    https://gitlab.inria.fr/lasgoutt/parameters
	Copyright (C) Jean-Marc Lasgouttes 2014-2019.

	This generic class is used to provide access to software
	parameters defined through the C preprocessor (usually defined in
	the Parameters-def.h file). All the parameters are accessible as
	normal members of the class.

	See README.md file for a description.

*/

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <utility>
#include <vector>

// ======================== Fixed classes =========================
// The classes ParametersBase and MultiParameters should only be defined once.

#ifndef PARAMETERS_H
#define PARAMETERS_H


// The abstract base class for parameters.
class ParametersBase {
public:
	friend class MultiParameters;

	ParametersBase () : comment_("#"), allow_unknown_(false), param_eq_value_(false) {}

	/**
	 * output a list of the parameters current values, along with
	 * a description.
	 */
	virtual void list(std::ostream & os) const = 0;


	void commentPrefix(std::string const & str)
	{
		comment_ = str;
	}


	std::string const & commentPrefix() const
	{
		return comment_;
	}


	void paramEqValue(bool b)
	{
		param_eq_value_ = b;
	}

	bool paramEqValue() const
	{
		return param_eq_value_;
	}

	void allowUnknown(bool b)
	{
		allow_unknown_ = b;
	}

	bool allowUnknown() const
	{
		return allow_unknown_;
	}

	/** Parse the known parameters of the command line
	 *
	 * The parameters that are recognized are (when \c allow_unknown
	 * is false)
	 *   -h           displays help and exits
	 *
	 *   param=value (when \c param_eq_value is true)
	 *   -param value (when \c param_eq_value is false)
	 *                sets parameter "param" to value "value".
	 *                The program exits if the argument is unknown.
	 *
	 * If \c allow_unknown is true, the remaining arguments have
	 * to be handled by the application. Otherwise, they trigger
	 * an error.
	 */
	void parse(int & argc, char ** argv)
	{
		parse_helper(argc, argv, *this);
	}

	/** Read the known parameters of from file \c fname
	 *
	 * The format of the file is
	 *   param1 value
	 *   param2 value
	 *   ...
	 */
	virtual bool read(std::string const & fname)
	{
		std::ifstream ifs(fname.c_str());
		if (!ifs.good())
			return false;

		while (ifs.good()) {
			std::string line, param, value;
			std::getline(ifs, line);
			// remove comment
			size_t const com_pos = line.find(comment_);
			line = line.substr(0, com_pos);
			if (line.find_first_not_of(" \t") == std::string::npos)
				continue;
			std::istringstream iss(line);
			iss >> param >> value;
			set(param, value);
		}
		return true;
	}

protected:

	// Helper function that can output two sets of strings in two
	// columns with correct spacing.
	class OutputTable {
	public:
		OutputTable() : maxlen_(0) {}

		// insert two strings at end of table. If second string is
		// empty, the first string is allowed to span the two columns.
		void add(std::string const & c, std::string const & d = std::string()) {
			if (!d.empty() && c.length() > maxlen_)
				maxlen_ = c.length();
			data_.push_back(std::make_pair(c, d));
		}

		// insert one element at any place.
		void insert(size_t i, std::string const & c) {
			data_.insert(data_.begin() + i, std::make_pair(c, std::string()));
		}

		size_t size() const { return data_.size(); }

		// the real work. The margin is the minimal number of spaces
		// between the columns.
		void output(std::ostream & os, size_t margin = 5) const {
			for (size_t i = 0 ; i < data_.size() ; ++i) {
				os << data_[i].first;
				if (!data_[i].second.empty())
					os << std::string(margin + maxlen_ - data_[i].first.length(), ' ')
					   << data_[i].second;
				os << '\n';
			}
		}

	private:
		std::vector<std::pair<std::string, std::string> > data_;
		size_t maxlen_;
	};

	virtual void help_switch(OutputTable &, std::string const &) const = 0;

	virtual void help_values(OutputTable &, std::string const &) const = 0;


	virtual bool set(std::string const & /*param*/, std::string const & /*value*/)
	{
		return false;
	}

	virtual bool set_enum(std::string const & /*param*/)
	{
		return false;
	}

private:

	void help(std::string const & prog) const
	{
		OutputTable table;
		if (param_eq_value_) {
			table.add("Usage: " + prog + " [param=value]...");
		} else {
			table.add("Usage: " + prog + " [option]... [-param value]...");
			help_switch(table, "\nAvailable options (* marks default setting):");
		}
		help_values(table, "\nAvailable parameters (along with default values):");
		table.output(std::cerr);
	}


	int parse_param_eq_value(int const i, int /*argc*/, char ** argv)
	{
		std::string const arg = argv[i];
		std::string name, value;
		size_t pos = arg.find("=");
		if (pos != std::string::npos) {
			name = arg.substr(0, pos);
			value = arg.substr(pos + 1);
			if (set(name, value))
				return 1;
			else
				return 0;
		} else
			return 0;
	}


	int parse_minus_param_value(int const i, int argc, char ** argv)
	{
		std::string const arg = argv[i];
		if (arg.empty())
			return 0;
		if (arg[0] == '-' && i + 1 < argc) {
			std::string name = arg.substr(1);
			std::string value = argv[i + 1];
			if (set(name, value))
				return 2;
			else
				return 0;
		} else
			return 0;
	}

	int parse_switch(int const i, int /*argc*/, char ** argv)
	{
		std::string const arg = argv[i];
		if (arg.empty())
			return 0;
		if (arg[0] == '-') {
			std::string name = arg.substr(1);
			if (set_enum(name))
				return 1;
			else
				return 0;
		} else
			return 0;
	}

protected:

	static std::string lc_(std::string name) {
		for (size_t i = 0 ; i < name.size() ; ++i)
			if (name[i] >= 'A' && name[i] <= 'Z')
				name[i] += 'a' - 'A';
		return name;
	}

	static bool matches_(std::string const & name, std::string const & param) {
		return lc_(name).substr(0,param.length()) == lc_(param);
	}

	// The actual parse workhorse. Put here because it can be called
	// directly by the MultiParameters class.
	void parse_helper(int & argc, char ** argv, ParametersBase const & ref)
	{
		int i = 1;
		while (i < argc) {
			if ((std::string(argv[i]) == "-h" || std::string(argv[i]) == "-help")
			    && !allow_unknown_) {
				ref.help(argv[0]);
				exit(0);
			}

			if (!param_eq_value_) {
				int const num = parse_switch(i, argc, argv);
				if (num > 0) {
					if (!allow_unknown_) {
						i += num;
						continue;
					}
					for (int j = i + num ; j < argc ; ++j)
						argv[j - num] = argv[j];
					argc -= num;
					continue;
				}
			}

			int const num = param_eq_value_ ?
			                parse_param_eq_value(i, argc, argv)
			                : parse_minus_param_value(i, argc, argv);
			if (num > 0) {
				if (allow_unknown_) {
					for (int j = i + num ; j < argc ; ++j)
						argv[j - num] = argv[j];
					argc -= num;
				} else
					i += num;
			} else if (allow_unknown_)
				++i;
			else {
				std::cerr << "ERROR: argument `" << argv[i]
				          << "' cannot be parsed." << std::endl;
				exit(1);
			}
		}
	}

	// The comment character
	std::string comment_;
	bool allow_unknown_;
	bool param_eq_value_;
};


// The container class that can glue several parameters together
class MultiParameters : public ParametersBase
{
public:
	MultiParameters() {}

	void add(ParametersBase & p)
	{
		list_.push_back(&p);
	}


	virtual void list(std::ostream & os) const
	{
		for (size_t i = 0 ; i < list_.size() ; ++i)
			list_[i]->list(os);
	}


	void parse(int & argc, char ** argv)
	{
		bool const save_au = allow_unknown_;
		allow_unknown_ = true;
		for (size_t i = 0 ; i < list_.size() ; ++i)
			list_[i]->parse_helper(argc, argv, *this);
		allow_unknown_ = save_au;
		if (!allow_unknown_)
			ParametersBase::parse_helper(argc, argv, *this);
	}


	virtual bool read(std::string const & fname)
	{
		// currently Parameters::read only fails when file is not
		// found. Therefore the result is always the same in the loop.
		bool ret = true;
		for (size_t i = 0 ; i < list_.size() ; ++i)
			ret |= list_[i]->read(fname);
		return ret;
	}


protected:

	virtual void help_switch(OutputTable & table, std::string const & title) const
	{
		std::cerr << "Nlist=" <<list_.size() <<std::endl;
		for (size_t i = 0 ; i < list_.size() ; ++i)
			list_[i]->help_switch(table, title);
	}

	virtual void help_values(OutputTable & table, std::string const & title) const
	{
		for (size_t i = 0 ; i < list_.size() ; ++i)
			list_[i]->help_values(table, title);
	}

private:
	// The list of parameters objects handled by the class
	std::vector<ParametersBase *> list_;
};


#endif //PARAMETERS_H


// ===================== Preprocessor-controlled class ====================
// this part shall be compiled at every #include.

#ifndef PARAMETERS_CLASS
#define PARAMETERS_CLASS Parameters
#define _CLEAN_P_C 1
#endif

#ifndef PARAMETERS_DEF
#define PARAMETERS_DEF "Parameters-def.h"
#define _CLEAN_P_D 1
#endif

#define PARAM(TYPE, NAME, DEFAULT, DESC)
#define VALUE_ENUM(NAME, VALUE, DESC)
#define PARAM_ENUM(TYPE, NAME, DEFAULT, DESC)
#define HELP(HELP_STRING)
#define IN_PREAMBLE
#include PARAMETERS_DEF
#undef PARAM
#undef PARAM_ENUM
#undef VALUE_ENUM
#undef HELP
#undef IN_PREAMBLE


class PARAMETERS_CLASS : public ParametersBase {
public:
	PARAMETERS_CLASS() {
#	define PARAM(TYPE, NAME, DEFAULT, DESC) \
		NAME = DEFAULT;
#	define VALUE_ENUM(NAME, VALUE, DESC)
#	define PARAM_ENUM(TYPE, NAME, DEFAULT, DESC)			\
		NAME = DEFAULT;
#	define HELP(HELP_STRING)
#	include PARAMETERS_DEF
#	undef PARAM
#	undef VALUE_ENUM
#	undef PARAM_ENUM
#	undef HELP
	}

	virtual void list(std::ostream & os) const
	{
		OutputTable table;
		std::ostringstream oss;

#	define PARAM(TYPE, NAME, DEFAULT, DESC) \
		oss.str(std::string());		\
		oss << NAME; \
		table.add(lc_(#NAME) + " = " + oss.str(), comment_ + " " + DESC);
#	define VALUE_ENUM(NAME, VALUE, DESC) \
		if (NAME == VALUE) \
			table.add(lc_(#NAME) + " = " + lc_(#VALUE), comment_ + " " + DESC);
#	define PARAM_ENUM(TYPE, NAME, DEFAULT, DESC)
#	define HELP(HELP_STRING) \
		table.add("\n" + comment_ + " " + HELP_STRING);
#	include PARAMETERS_DEF
#	undef PARAM
#	undef VALUE_ENUM
#	undef PARAM_ENUM
#	undef HELP

		table.output(os);
	}


protected:

	virtual void help_switch(OutputTable & table, std::string const & title) const
	{
		size_t const front = table.size();

#	define PARAM(TYPE, NAME, DEFAULT, DESC)
#	define VALUE_ENUM(NAME, VALUE, DESC) \
		table.add("  -" + lc_(#VALUE) + (NAME == VALUE ? "*" : ""), \
				  DESC);
#	define PARAM_ENUM(TYPE, NAME, DEFAULT, DESC) \
		table.add("\n " DESC);
#	define HELP(HELP_STRING)
#	include PARAMETERS_DEF
#	undef PARAM
#	undef VALUE_ENUM
#	undef PARAM_ENUM
#	undef HELP

		if (table.size() != front)
			table.insert(front, title);
	}

	virtual void help_values(OutputTable & table, std::string const & title) const
	{
		std::ostringstream oss;
		size_t const front = table.size();

#	define PARAM(TYPE, NAME, DEFAULT, DESC)		\
		oss.str(std::string());		\
		oss << DEFAULT; \
		if (param_eq_value_) \
			table.add("  " + lc_(#NAME) + "[=" + oss.str() + "]", DESC); \
		else \
			table.add("  -" + lc_(#NAME) + " [" + oss.str() + "]", DESC);
#	define VALUE_ENUM(NAME, VALUE, DESC) \
		if (param_eq_value_) \
			table.add("    " + lc_(#VALUE), DESC);
#	define PARAM_ENUM(TYPE, NAME, DEFAULT, DESC)			\
		if (param_eq_value_) \
			table.add("  " + lc_(#NAME) + "[=" + lc_(#DEFAULT) + "]", DESC);
#	define HELP(HELP_STRING) \
		table.add("\n " HELP_STRING);
#	include PARAMETERS_DEF
#	undef PARAM
#	undef VALUE_ENUM
#	undef PARAM_ENUM
#	undef HELP

		if (table.size() != front)
			table.insert(front, title);
	}


	virtual bool set(std::string const & param, std::string const & value)
	{
		std::string varname;
		std::istringstream iss(value);

#	define PARAM(TYPE, NAME, DEFAULT, DESC) \
		if (matches_(#NAME, param)) { \
			if (varname.empty()) { \
				iss >> NAME; \
				if (iss.fail()) { ; \
					std::cerr << "ERROR: parameter `" << param \
							  << "' cannot have value `" << value << "'." \
						  << std::endl; \
					exit(1); \
				} \
				varname = lc_(#NAME); \
			} else { \
				std::cerr << "ERROR: variable name `" << param \
				          << "' matches both `" << varname << "' and `" \
				          << lc_(#NAME) << "'." << std::endl; \
				exit(1); \
			} \
		}
#	define VALUE_ENUM(NAME, VALUE, DESC) \
		if (matches_(#NAME, param) && matches_(#VALUE, value)) { \
			if (varname.empty()) { \
				NAME = VALUE; \
				varname = lc_(#NAME); \
			} else { \
				std::cerr << "ERROR: variable name `" << param \
				          << "' matches both `" << varname << "' and `" \
				          << lc_(#NAME) << "'." << std::endl; \
				exit(1); \
			} \
		}
#	define PARAM_ENUM(TYPE, NAME, DEFAULT, DESC)
#	define HELP(HELP_STRING)
#	include PARAMETERS_DEF
#	undef PARAM
#	undef VALUE_ENUM
#	undef PARAM_ENUM
#	undef HELP

		return !varname.empty();
	}


	virtual bool set_enum(std::string const & param)
	{
		std::string valname;

#	define PARAM(TYPE, NAME, DEFAULT, DESC)
#	define VALUE_ENUM(NAME, VALUE, DESC) \
		if (matches_(#VALUE, param)) { \
			if (!NAME##_value_.empty() && NAME##_value_ != lc_(#VALUE)) { \
				/* we have set this enum already */ \
				std::cerr << "ERROR: options `-" << lc_(#VALUE) << "' and `-" \
				          << NAME##_value_ << "' cannot be used simultaneously." \
				          << std::endl;									\
				exit(1); \
			} else if (!valname.empty()) { \
				std::cerr << "ERROR: option `-" << param \
				          << "' matches both `-" << valname << "' and `-" \
				          << lc_(#VALUE) << "'." << std::endl; \
				exit(1); \
			} else { \
				NAME = VALUE; \
				NAME##_value_ = lc_(#VALUE); \
				valname = lc_(#VALUE); \
			} \
		}
#	define PARAM_ENUM(TYPE, NAME, DEFAULT, DESC)
#	define HELP(HELP_STRING)
#	include PARAMETERS_DEF
#	undef PARAM
#	undef VALUE_ENUM
#	undef PARAM_ENUM
#	undef HELP
		(void)param;
		return !valname.empty();
	}


public:

	// Configurable parameters
#	define PARAM(TYPE, NAME, DEFAULT, DESC) \
	TYPE NAME;
#	define VALUE_ENUM(NAME, VALUE, DESC)
#	define PARAM_ENUM(TYPE, NAME, DEFAULT, DESC) \
	TYPE NAME; \
	std::string NAME##_value_;
#	define HELP(HELP_STRING)
#	include PARAMETERS_DEF
#	undef PARAM
#	undef VALUE_ENUM
#	undef PARAM_ENUM
#	undef HELP
};

#ifdef _CLEAN_P_D
#undef PARAMETERS_DEF
#undef _CLEAN_P_D
#endif

#ifdef _CLEAN_P_C
#undef PARAMETERS_CLASS
#undef _CLEAN_P_C
#endif
