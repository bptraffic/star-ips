/*
  This file is input multiple times by Parameters.h in order to define
  the parameters accepted by the program. The parameters of the macro
  are as follows:

   PARAM(type, name, value, description)
   - type (bool, string, int...): the type of the underlying variable,
     (the type should be readable and writable by a stream)
   - name: the name of the parameter (used both in config files and as
     name in the code).
   - value: the default value of the parameter.
   - description: a help text

   To add a new parameter, it is enough to change this file.
 */
#ifdef IN_PREAMBLE
// Spectral constraints to be imposed on estimated precision matrix.
enum SConstraints {
	NONE,				// No constraint.
	WS,				// Walk-summability.
	WWS,				// Weak walk-summability (rho(R')<1).
	GSC				// General spectral constraints on R.
};
enum CouplingsFile {
  lastcouplings,
  allcouplings
};
#endif

PARAM_ENUM(CouplingsFile, couplingsfile, lastcouplings,
    "couplings*.dat files to output. One of:")
VALUE_ENUM(couplingsfile, allcouplings, "Write intermediary couplings files as well as last")
VALUE_ENUM(couplingsfile, lastcouplings, "Only write last couplings file")

PARAM_ENUM(SConstraints,sconstraints,NONE,
    "Type of constraint on new edges added to graph. One of:")
VALUE_ENUM(sconstraints,NONE,"No constraint.")
VALUE_ENUM(sconstraints,WS, "Enforce walk-summability.")
VALUE_ENUM(sconstraints,WWS, "Enforce weak walk-summability (rho(R')<1).")
VALUE_ENUM(sconstraints,GSC, "Enforce general spectral constraints on R.")

HELP("General settings")
PARAM (std::string, Config_file, "",
       "Configuration file containing the parameters values. Command line has the priority.")
PARAM (std::string, write_config_file, "", "If not empty, write the configuration to this file");
PARAM (int, VERBOSITY, 0, "Verbosity level for debugging purpose (0,1,2).")

HELP("Model definition")
PARAM (std::string, Cov_file, "", "Covariance matrix file to be used.")
PARAM (std::string, Pmat_file, "", "True precision matrix of the model (optional).")
PARAM (std::string, PmatInit_file, "", "Precision Matrix file to be used for initialization.")
PARAM (int, Nv, 0, "Dimension of the Gaussian vector.")

HELP("Algorithm control")
PARAM (double, K_obj, 0, "Maximum connectivity to reach.")
PARAM (double, K_th, 1,
       "Connectivity increment starting a link-updates step.")
PARAM (int, LOOP_SIZE, 0,
       "Minimal size of tolerated loops in IPS. 0: no constraints; >0: no loops; <0: only non-frustrated loops.")
PARAM (int, NLU, 1e5,
       "Maximal number of iterations for the link-update step.")
PARAM (double, LL_BOUND, 20, "Dual bound for the update step.")
PARAM (double, DLL_TH, 1e-6, "Stopping criterion on the log-likelihood.")
PARAM (int, NI, 1e5, "Maximal number of iterations.")
PARAM (double, spLowBound, -0.99,
       "Lower bound for general spectral constraints.")
PARAM (double, spUpBound, 0.99,
       "Upper bound for general spectral constraints.")
PARAM (double, ADD_PENALTY, 0, "Link addition penalty.")
PARAM (int, MODE, 0,"Mode used, 0 is the standard one with all computations done, 1 performs less computations.")
PARAM (int, Ny,0,"For a forecast model P(Y|X): number of variables Y out of Nv= NvX+NvY.")
PARAM (double,K_XY,-1,"For a forecast model P(Y|X): target connectivity contribution of links X-X and Y-Y before switching to X-Y. Default value -1 to discard this.")
PARAM (double,links_selection,1,"Fraction of pre-selected links based on their MI score at the beginning. The Others are discarded during the optimization.")
