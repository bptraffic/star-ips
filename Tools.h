// -*- C++ -*-
/**
 * This file is part of:
 *    star-ips -- a family of incremental GMRF estimation algorithms.
 *
 * Authors:
 *   Cyril Furtlehner, cyril.furtlehner@inria.fr
 *   Victorin Martin, victorin.martin@gmail.com
 *   Jean-Marc Lasgouttes, jean-marc.lasgouttes@inria.fr
 *
 * This software is distributed under the Creative Commons License
 * BY-NC-SA. The complete license can be found here
 *   http://creativecommons.org/licenses/by-nc-sa/4.0/legalcode
 */

#ifndef TOOLS_H
#define TOOLS_H

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <sstream>
#include <algorithm>
#include <cstring>
#include <numeric>
#include <limits>
#include <queue>

#include "simd_math_prims.h"
#include "Eigen/Core"

#define POSITIVE_TH 1e-6

//Dimension 2 vector.
template < typename T > class vector2
{
public:
	vector2 () {
	};

	vector2 (int rows, int cols) : data_(rows, std::vector < T > (cols)) {}

	inline std::vector < T > &operator[] (int i) {
		return data_[i];
	}

	inline const std::vector < T > &operator[] (int i) const {
		return data_[i];
	}

	size_t size1() const {
		return data_.size();
	}

	size_t size2() const {
		return data_.empty() ? 0 : data_.front().size();
	}

	void resize (int rows, int cols) {
		data_.resize (rows);
		for (int i = 0; i < rows; ++i)
			data_[i].resize (cols);
	}

	void resize (int rows, int cols, T value) {
		data_.resize (rows);
		for (int i = 0; i < rows; ++i)
			data_[i].resize (cols, value);
	}

private:
	std::vector < std::vector < T > >data_;
};

///Structure for roots of a degree two polynomial.
struct p2_roots {
	double r1, r2;
	bool are_roots_real;

	// Is there at least one root between a and b?
	bool in_range (double a, double b) {
		if (!are_roots_real)
			return false;
		return (r1 < b && r1 > a) || (r2 < b && r2 > a);
	}
};

// Compute roots of a degree 2 polynomial
p2_roots find_roots (double, double, double);

///
Eigen::MatrixXd::Scalar logdet(const Eigen::MatrixXd & M, bool use_cholesky = false);
bool import_matrix(Eigen::MatrixXd & M, std::string const & s);
bool export_matrix(Eigen::MatrixXd const & M, std::string const & s);
void compare_graph(Eigen::MatrixXd const &, Eigen::MatrixXd const &, std::ostream &);

#endif // TOOLS_H
