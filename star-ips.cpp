/**
 * This file is part of:
 *    star-ips -- a family of incremental GMRF estimation algorithms.
 *
 * Authors:
 *   Cyril Furtlehner, cyril.furtlehner@inria.fr
 *   Victorin Martin, victorin.martin@gmail.com
 *   Jean-Marc Lasgouttes, jean-marc.lasgouttes@inria.fr
 *
 * This software is distributed under the Creative Commons License
 * BY-NC-SA. The complete license can be found here
 *   http://creativecommons.org/licenses/by-nc-sa/4.0/legalcode
 */


#include "GMRF.h"
#include "IPSMethod.h"
#include "Parameters.h"

#include "Eigen/Core"
#include "Eigen/Eigenvalues"

using namespace std;
using namespace Eigen;

Parameters p;

static string get_name(Parameters const & p)
{
	ostringstream out;
	out << "LL";
	switch (p.sconstraints) {
	case WS:
		out << "_WS";
		break;
	case WWS:
		out << "_WWS";
		break;
	case GSC:
		out << "_GSC";
		break;
	default:
		break;
	}
	if (p.LOOP_SIZE < 0)
		out << "_FLOOP" << p.LOOP_SIZE;
	else if (p.LOOP_SIZE > 0)
		out << "_LOOP" << p.LOOP_SIZE;
	out << ".dat";
	return out.str();
}


void exit_status(ostream& os, int i, double x)
{
	os << "Exiting: ";
	if (p.K_obj <= TheModel->Km)
		os << "objective connectivity " << p.K_obj << " reached ("
		   << TheModel->Km << ")" << endl;
	else if (p.NI == i)
		os << "maximum number of iterations reached "
		   << i << "/" << p.NI << endl;
	else
		os << "likelihood increment is too small " << x << " < "
		   << p.DLL_TH << endl;

}


static bool param_validity(Parameters const & p)
{
	if (p.Nv==0) {
		cerr << "Number of variables not correctly initialized!" << endl;
		return false;
	}
	if (p.Cov_file.empty()) {
		cerr << "No file given for the covariance matrix!" << endl;
		return false;
	}
	return true;
}


void output_header(ostream& os, bool bReal_Cov)
{
	string obj = (p.MODE==0)? "log-likelihood(ll)" : "log-likelihood variation(ll)";
	os << "connectivity" << "  " << obj << "  " << "max(ll)";
	if(bReal_Cov)
		os << "  " << "true-ll" << "  " << "max(true ll)" << "  " << "ll(true matrix)";
	os << endl;
}


void output_results(ostream& os,double ll, double increase,
                    double max_ll, double tll, double max_tll, double llra, bool bReal_Cov)
{
	double const obj = (p.MODE==0)? ll : increase;
	os << TheModel->Km << "\t" << obj << "\t" <<
		max_ll;
	if (bReal_Cov)
		os << "\t" << tll << "\t" << max_tll << "\t" << llra;
	os << endl;
}


void export_model(MatrixXd const & A, double Km = -1)
{
	if (Km < 0)
		export_matrix(A, "couplings.dat");
	else if (p.couplingsfile == allcouplings) {
		stringstream ss;
		ss.clear();
		ss << fixed << setprecision(2) << "couplings_" << Km << ".dat";
		export_matrix(A, ss.str());
	}
}


int update_existing_links(double& increase, double& ll)
{
	int iter = 0;
	double bound = get_LL_bound();
	if (p.VERBOSITY > 1) {
		cout << "#updates\t bound\t LL" << endl;
		cout << iter << "\t" <<	bound << "\t" << increase <<	endl;
	}
	while ( (bound > p.LL_BOUND || bound < -1e-8) && (iter < p.NLU)) {
		double eps = add_maxLL_link(1);
		increase += eps;
		ll += eps;
		iter++;
		if ( p.sconstraints == NONE ) {
			int const n = wLL[0][1];
			int const j = wLL[0][2];
			int const m = TheModel->Vois[n][j];
			double bu = block_update(n);
			bu += block_update(m);
			increase += bu;
			ll += bu;
		}
		if (iter % 100 == 0) {
			bound = get_LL_bound();
			if (p.VERBOSITY > 1) {
				cout << iter << "\t" << bound << "\t" << increase << endl;
			}
		}
	}
	if ( (p.VERBOSITY > 1) && (iter % 100 != 0) ) {
		cout << iter << "\t" << bound << "\t" << increase << endl;
	}
	return iter;
}


int main(int argc, char **argv)
{
	// Default setting for parsing is:
	// * do not allow unknown parameters
	// * options in the form -nv 100
	p.parse(argc, argv);
	if (!p.Config_file.empty()) {
		//To keep priority to command line arguments.
		p.read(p.Config_file);
		p.parse(argc, argv);
	}
	if (!param_validity(p)) {
		return -1;
	}

	if (p.VERBOSITY>0)
		p.list(cout);

	if (!p.write_config_file.empty()) {
		ofstream ofs(p.write_config_file.c_str());
		// we do not want this value in the config file.
		p.write_config_file.clear();
		p.list(ofs);
		return 0;
	}

	ofstream ofs, graph_comp;
	double llra = 0, tll = 0, max_tll = 0, ll = 0,
		max_ll = 0, increase = 0;
	MatrixXd Real_A, Real_Cov;
	bool bReal_A = false, bReal_Cov = false;

	// Load the covariance matrix.
	C.resize(p.Nv, p.Nv);
	if (!import_matrix(C, p.Cov_file)) {
		cerr << "Error when importing the covariance matrix from "
		     << p.Cov_file << endl;
		return -2;
	}
	// Load the vector.
	MatrixXd IC;
	if (p.MODE == 0)
		IC = C.inverse();
	else {
		IC = MatrixXd::Constant(p.Nv, p.Nv, 1);
		for (int n = 0; n < p.Nv; n++)
			IC(n, n) = 1 / C(n, n);
	}
	TheModel = new GMRF(IC);
	if (p.PmatInit_file.empty())
		TheModel->prune_all();
	else {                            //load the Precision matrix and initialize the model
		A.resize(p.Nv, p.Nv);
		if (!import_matrix(A, p.PmatInit_file)) {
			cerr << "Error when importing the Precision matrix from "
				 << p.PmatInit_file << endl;
			return -2;
		}
		TheModel->prune_from_Pmatrix(A);
	}
	init_matrices();
	// release IC memory. If we do it earlier, the program gets much slower (??)
	IC.resize(1, 1);
	if (p.MODE == 0) {
		// Identical to get_LL(IC, C) = logdet(IC) -(C*IC).trace()
		max_ll = -logdet(C) - p.Nv;
		ll = get_LL(A, C);
	}
	if (p.Ny >0 && p.K_XY >0)   //For a forecast model P(Y|X), set to first stage of internal links (X-X and Y-Y) creation
		TheModel->set_XX_YY_Mask(p.Nv-p.Ny,p.Ny);

	if (!p.Pmat_file.empty()) {	///Load the true precision matrix and compute the true covariance matrix.
		bReal_A = true;
		Real_A.resize(p.Nv,p.Nv);
		if (!import_matrix(Real_A, p.Pmat_file)) {
			cerr << "Error when importing the true precision matrix from "
			     << p.Pmat_file << endl;
			return -2;
		}
		if (p.MODE == 0) {
			bReal_Cov = true;
			Real_Cov = Real_A.inverse();
			// Identical to get_LL(Real_A, Real_Cov) = logdet(Real_A) - (Real_A*Real_Cov).trace()
			max_tll = logdet(Real_A) - p.Nv;
			tll = get_LL(A, Real_Cov);
			llra = get_LL(Real_A, C);
		}
		graph_comp.open("graph_comp.dat");
		compare_graph(Real_A, A, graph_comp);
	}
	string iss_name = get_name(p);
	ofs.open(iss_name.c_str());

	output_header(cout,bReal_Cov);
	output_results(cout,ll,increase,max_ll,tll,max_tll,llra,bReal_Cov);
	output_results(ofs,ll,increase,max_ll,tll,max_tll,llra,bReal_Cov);

	export_model(A, TheModel->Km);
	double okm = TheModel->Km;

	int i = 0;
	bool cond = (TheModel->Km < p.K_obj) && (i < p.NI);
	//-----------------------------------             MAIN LOOP       --------------------------
	while (cond) {
		if (i==0)
			pre_select_links(p.links_selection);
		i++;
		double const x = add_maxLL_link(0);
		ll += x;
		increase += x;
		if (wLL[0][3] == 1) {	///link addition.
			if (bReal_A)
				compare_graph(Real_A, A, graph_comp);

			if ( p.sconstraints == NONE ) {
				int n = wLL[0][1];
				int j = wLL[0][2];
				int m = TheModel->Vois[n][j];
				double bu = block_update(n);
				bu += block_update(m);
				increase += bu;
				ll += bu;
			}

		}
		if (wLL[0][3] == -1) {	///link deletion.
			if ( p.sconstraints != NONE ) {
				int iter = 0;
				double eps = 1;
				while (eps > 1e-3 && iter < p.NLU) {
					eps = add_maxLL_link(1);
					increase += eps;
					ll += eps;
					iter++;
				}
			} else {
				int const n = wLL[0][1];
				int const j = wLL[0][2];
				int const m = TheModel->Vois[n][j];
				double bu = block_update(n);
				bu += block_update(m);
				increase += bu;
				ll += bu;
			}
			if (bReal_A)
				compare_graph(Real_A, A, graph_comp);

		}
		if (x <= 0 && wLL[0][3] >= 0) {
			i = p.NI;
			if (p.sconstraints == WS)
				cout << "Limit of Walk-Summability reached!" <<
					endl;
			else if (p.sconstraints == WWS)
				cout <<
					"Limit of Weak Walk-Summability reached!" <<
					endl;
			else if (p.sconstraints == GSC)
				cout << "Limit of Spectral constraints reached!" <<
					endl;
			else if (p.LOOP_SIZE > 0)
				cout << "Limit with no loop of size " << p.LOOP_SIZE <<
					"reached!" << endl;
			else if (p.LOOP_SIZE < 0)
				cout << "Limit with no frustrated loop of size " << -p.LOOP_SIZE <<
					"reached!" << endl;
			else
				cout <<
					"No modifications increasing the likelihood found!"
				     << endl;
		}
		if (TheModel->Km - okm > p.K_th) {	// We switch to optimizing the existing edges without addition (or deletion)
			int iter = update_existing_links(increase, ll);
			if (p.VERBOSITY > 0)
				cout << "updating existing links took " << iter  << " iterations" << endl;
			if (p.MODE == 0)
				ll = get_LL(A, C);
			if (p.Ny>0 && TheModel->Km > p.K_obj-p.K_XY && okm < p.K_obj-p.K_XY && p.K_XY > 0)    //for a forecast model P(Y|X): switch to inter X-Y links production 
				TheModel->set_XY_Mask(p.Nv-p.Ny,p.Ny);
			if (ll == -1e10)
				i = p.NI;
			else {
				if (bReal_Cov)
					tll = get_LL(A, Real_Cov);
				output_results(cout,ll,increase,max_ll,tll,max_tll,llra,bReal_Cov);
				output_results(ofs,ll,increase,max_ll,tll,max_tll,llra,bReal_Cov);

				okm = TheModel->Km;
				export_model(A, TheModel->Km);
			}
		}
		if (p.VERBOSITY > 1)
			cout << TheModel->Km << "\t" << ll << endl;

		cond = (TheModel->Km < p.K_obj) && (i < p.NI) && !((0 <= x) && (x < p.DLL_TH));
		if (!cond)
			exit_status(cout, i, x);
	}
	if (p.MODE == 0)
		ll = get_LL(A, C);
	if (bReal_Cov)
		tll = get_LL(A, Real_Cov);
	output_results(cout,ll,increase,max_ll,tll,max_tll,llra,bReal_Cov);
	output_results(ofs,ll,increase,max_ll,tll,max_tll,llra,bReal_Cov);
	export_model(A);

	if (bReal_A)
		compare_graph(Real_A, A, graph_comp);

	ofs.close();
	graph_comp.close();
	return 0;
}
