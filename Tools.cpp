/**
 * This file is part of:
 *    star-ips -- a family of incremental GMRF estimation algorithms.
 *
 * Authors:
 *   Cyril Furtlehner, cyril.furtlehner@inria.fr
 *   Victorin Martin, victorin.martin@gmail.com
 *   Jean-Marc Lasgouttes, jean-marc.lasgouttes@inria.fr
 *
 * This software is distributed under the Creative Commons License
 * BY-NC-SA. The complete license can be found here
 *   http://creativecommons.org/licenses/by-nc-sa/4.0/legalcode
 */

#include "Tools.h"

#include "Eigen/Eigenvalues"

using namespace std;
using namespace Eigen;

// Finding (only real) Roots of degree two polynomial.
p2_roots find_roots(double a, double b, double c)
{
	struct p2_roots roots = { -1e8, 1e8, false };
	if (fabs(b / a) > 1e10) {	//A degree one polynomial.
		if (fabs(b) < 1e-20)
			return roots;
		roots.are_roots_real = true;
		roots.r1 = -c / b;
		roots.r2 = roots.r1;
		return roots;
	}
	double const delta = b * b - 4 * a * c;
	if (delta < 0) {
		return roots;
	}
	double const sqrtdelta = sqrt(delta);
	roots.are_roots_real = true;
	roots.r1 = (-b - sqrtdelta) / (2 * a);
	roots.r2 = (-b + sqrtdelta) / (2 * a);
	return roots;
}

// From https://gist.github.com/redpony/fc8a0db6b20f7b1a3f23
// set use_cholesky if M is symmetric - it's faster and more stable
MatrixXd::Scalar logdet(const MatrixXd & M, bool use_cholesky) {
	typedef MatrixXd::Scalar Scalar;
	Scalar ld = 0;
	if (use_cholesky) {
		LLT<Matrix<Scalar,Dynamic,Dynamic>> chol(M);
		auto & U = chol.matrixL();
		for (int i = 0; i < M.rows(); ++i)
			ld += logapprox(U(i,i));
		ld *= 2;
	} else {
		PartialPivLU<Matrix<Scalar,Dynamic,Dynamic>> lu(M);
		auto & LU = lu.matrixLU();
		Scalar c = lu.permutationP().determinant(); // -1 or 1
		for (int i = 0; i < LU.rows(); ++i) {
			const auto & lii = LU(i,i);
			if (lii < Scalar(0)) c *= -1;
			ld += logapprox(abs(lii));
		}
		ld += logapprox(c);
	}
	return ld;
}


///**** Read a (square) matrix from a file (M should be of the right size) **************
bool import_matrix(MatrixXd & M, string const & s)
{
	int const n = M.rows(), m = M.cols();
	double mij;
	if (n != m) {
		cerr << "Not a square matrix in import_matrix" << endl;
		return false;
	}
	ifstream from_file;
	from_file.open(s.c_str());
	if (!from_file.is_open())
		return false;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			from_file >> mij;
			M(i, j) = mij;
		}
	}
	return true;
}

//FIXME: use format()?
bool export_matrix(MatrixXd const & M, string const & s)
{
	ofstream ofs;
	ofs.open(s.c_str());
	if (!ofs.is_open())
		return false;

	for (int i = 0; i < M.rows(); i++) {
		for (int j = 0; j < M.cols(); j++)
			ofs << M(i, j) << " ";
		ofs << endl;
	}
	ofs.close();
	return true;
}


void compare_graph(MatrixXd const & MRef, MatrixXd const & M, ostream & os)
{
	size_t false_pos = 0;
	size_t false_neg = 0;
	size_t nedges = 0;
	for (int i = 0; i < MRef.rows(); i++) {
		for (int j = i + 1; j < MRef.cols(); j++) {
			bool isInRef = fabs(MRef(i, j)) > 1e-9;
			bool isInM = fabs(M(i, j)) > 1e-9;
			if (isInM)
				nedges++;
			if (isInM != isInRef) {
				if (isInRef)
					false_neg++;
				else
					false_pos++;
			}
		}
	}
	double k = (2.0*nedges)/(1.0*M.rows());
	os << k << "\t" << false_pos << "\t"
	   << false_neg << "\t" << nedges << endl;
}
