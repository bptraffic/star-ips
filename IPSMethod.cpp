/**
 * This file is part of:
 *    star-ips -- a family of incremental GMRF estimation algorithms.
 *
 * Authors:
 *   Cyril Furtlehner, cyril.furtlehner@inria.fr
 *   Victorin Martin, victorin.martin@gmail.com
 *   Jean-Marc Lasgouttes, jean-marc.lasgouttes@inria.fr
 *
 * This software is distributed under the Creative Commons License
 * BY-NC-SA. The complete license can be found here
 *   http://creativecommons.org/licenses/by-nc-sa/4.0/legalcode
 */

#include "IPSMethod.h"

#include "GMRF.h"
#include "Parameters.h"

#include "Eigen/Eigenvalues"

extern Parameters p;

using namespace std;
using namespace Eigen;

double tll;
GMRF *TheModel;

MatrixXd A, IA, C;
/// Matrices related to spectral constraints. W is 'I-|R|' and S is
/// 'I+R' (changing all signs). IW and IS are the inverses.
MatrixXd W, IW;
MatrixXd S, IS;
///Matrices for generalized spectral contraints on R.
MatrixXd Sp, Sm, ISp, ISm;


vector2 < double >wLL;

/// Current state of the spectral constraints.
struct SC_state {
	double radius, factor;
	bool is_SC;
};



///------------------------------------------------------------------------
/// ------------------------- Matrices updates ----------------------------
///------------------------------------------------------------------------
bool update_inverse(int n, int m, MatrixXd & IM, Matrix2d & P)
{
	if (IM.rows() != IM.cols())
		return false;

	size_t const M_size = IM.rows();
	Matrix2d SIM;
	SIM << IM(n, n), IM(n, m),
		   IM(m, n), IM(m, m); //The (mn) submatrix of IM.
	Matrix2d IP = (P * SIM + Matrix2d::Identity()).inverse() * P;

	// Make copy of the two important columns of IM. Now we can modify
	// the matrix in place.
	MatrixX2d IMnm(M_size, 2);
	IMnm.col(0) = IM.col(n);
	IMnm.col(1) = IM.col(m);
	IM.noalias() -= IMnm * IP * IMnm.transpose();
	return true;
}


void update_matrices(int n, int m, double ai, double aj, double aij, double vi,
                double vj, double vij)
{
	A(n, n) = ai + vi;
	A(m, m) = aj + vj;
	A(n, m) = aij + vij;
	A(m, n) = aij + vij;
	switch (p.sconstraints) {
	case WWS:
		S(n, n) = ai + vi;
		S(m, m) = aj + vj;
		S(n, m) = -aij - vij;
		S(m, n) = -aij - vij;
		break;
	case WS:
		W(n, n) = ai + vi;
		W(m, m) = aj + vj;
		W(n, m) = -abs(aij + vij);
		W(m, n) = -abs(aij + vij);
		break;
	case GSC:
		Sp(n, n) = ai + vi;
		Sp(m, m) = aj + vj;
		Sp(n, m) = (aij + vij) / p.spUpBound;
		Sp(m, n) = (aij + vij) / p.spUpBound;
		Sm(n, n) = ai + vi;
		Sm(m, m) = aj + vj;
		Sm(n, m) = (aij + vij) / p.spLowBound;
		Sm(m, n) = (aij + vij) / p.spLowBound;
		break;
	default:
		break;
	}
}


void init_matrices()
{
	if (p.VERBOSITY > 1)
		cout << "build GMRF..." << flush;
	// is A already initialised?
	bool const PInit = A.rows() > 0;
	if (!PInit)
		A.setZero(p.Nv, p.Nv);

	switch (p.sconstraints) {
	case WS:
		W.setZero(p.Nv, p.Nv);
		break;
	case WWS:
		S.setZero(p.Nv, p.Nv);
		break;
	case GSC:
		Sp.setZero(p.Nv, p.Nv);
		Sm.setZero(p.Nv, p.Nv);
		break;
	default:
		break;
	}

	for (int i = 0; i < p.Nv; i++) {
		double const x = C(i, i);
		double aii;
		if (PInit)
			aii = A(i, i);
		else
			aii = 1.0 / x;
		for (int j = 0; j < TheModel->nv[i]; j++) {
			int const m = TheModel->Vois[i][j];
			double const y = C(m, m);
			double const z = C(i, m);
			double aij;
			if (PInit)
				aij = A(i, m);
			else {
				aij = z / (z * z - x * y);
				aii += y / (x * y - z * z) - 1.0 / x;
				A(i, m) = aij;
			}
			switch (p.sconstraints) {
			case WWS:
				S(i, m) = -aij;
				break;
			case WS:
				W(i, m) = -abs(aij);
				break;
			case GSC:
				Sp(i, m) = aij / p.spUpBound;
				Sm(i, m) = aij / p.spLowBound;
				break;
			default:
				break;
			}
		}
		if (!PInit)
			A(i, i) = aii;
		switch (p.sconstraints) {
		case WS:
			W(i, i) = aii;
			break;
		case WWS:
			S(i, i) = aii;
			break;
		case GSC:
			Sp(i, i) = aii;
			Sm(i, i) = aii;
			break;
		default:
			break;
		}

	}
	IA = A.inverse();
	switch (p.sconstraints) {
	case WWS:
		IS = S.inverse();
		break;
	case WS:
		IW = W.inverse();
		break;
	case GSC:
		ISp = Sp.inverse();
		ISm = Sm.inverse();
		break;
	default:
		break;
	}
}

void add_edge_to_blacklist(int n1, int j)
{
	int const n2 = TheModel->Vois[n1][j];
	int const ii = TheModel->ind[n1][j];
	TheModel->re_index(n1, j, TheModel->nv0[n1] - 1);
	TheModel->nv0[n1]--;
	TheModel->re_index(n2, ii, TheModel->nv0[n2] - 1);
	TheModel->nv0[n2]--;
}

/// ----------------------------------------------------------------------
/// -----------  METHODS RELATED TO SPECTRAL CONSTRAINTS -----------------
/// ----------------------------------------------------------------------


/// Check if the model (A) is Walk-Summable using eigenvalue decomposition.
SC_state is_WS_model()
{
	SC_state returnValue;
	int const n = A.rows();
	LLT<MatrixXd> llt(W); // compute the Cholesky decomposition of W
	returnValue.is_SC = llt.info() != Eigen::NumericalIssue;

	//FIXME: only the diagonal or R should be set to 0 (performance)
	MatrixXd R = MatrixXd::Zero(n, n);

	for (int i = 0; i < n; i++) {
		for (int j = i + 1; j < n; j++) {
			R(i, j) = abs(A(i, j) / sqrt(A(j, j) * A(i, i)));
			R(j, i) = abs(A(j, i) / sqrt(A(j, j) * A(i, i)));
		}
	}
	// FIXME: is R symmetric?
	SelfAdjointEigenSolver<MatrixXd> sol(R, false); // do not compute eigen vectors
	VectorXd lp = sol.eigenvalues();
	// FIXME: use directly maxCoeff
	double rho = 0;
	for (int i = 0; i < n; i++)
		rho = max(rho, abs(lp(i)));
	returnValue.radius = rho;

	if (!returnValue.is_SC)
		cout << "The model is not Walk-Summable (spectral radius  > " <<
			1 << "):\t" << rho << endl;
	if (p.VERBOSITY > 1)
		cout << "Real spectral radius: " << rho << endl;

	return returnValue;
}

///Check if the model remains WS after perturbation.
SC_state is_WS_after_perturbation(int n1, int n2)
{
	double const dm = 0.0, th = 0.98;
	struct SC_state sc_out = { -1.0, 1.0, false };
	double const A12 = A(n1, n2);
	double const IW11 = IW(n1, n1);
	double const IW22 = IW(n2, n2);
	double const IW12 = IW(n1, n2);
	double const detIW = IW11 * IW22 - IW12 * IW12;
	double const detC = C(n1, n1) * C(n2, n2) - C(n1, n2) * C(n2, n1);
	double const detIA = IA(n1, n1) * IA(n2, n2) - IA(n1, n2) * IA(n2, n1);
	double const V11 = C(n2, n2) / detC - IA(n2, n2) / detIA;
	double const V22 = C(n1, n1) / detC - IA(n1, n1) / detIA;
	double const V12 = IA(n1, n2) / detIA - C(n1, n2) / detC;

	double const a = detIW * (V11 * V22 - V12 * V12);
	double const b0 = IW11 * V11 + IW22 * V22;
	double const alpha_th = -A12 / V12;
	if (abs(A12) < 1e-6) {
		double const b = b0 - 2 * IW12 * abs(V12);
		double const c = 1;
		p2_roots rootsQ = find_roots(a, b, c);
		sc_out.factor = fmin(a + b + c, 1);
		sc_out.is_SC = !rootsQ.in_range(0, 1 + dm)
			&& (sc_out.factor > th);
		return sc_out;
	}
	if (A12 > 0) {
		double b = b0 - 2 * IW12 * V12;
		double c = 1;
		p2_roots rootsQ1a = find_roots(a, b, c);
		sc_out.factor = fmin(a + b + c, 1);
		if (V12 >= 0 || alpha_th > 1 + dm) {
			sc_out.is_SC = !rootsQ1a.in_range(0, 1 + dm)
				&& (sc_out.factor > th);
			return sc_out;
		}
		b = b0 + 2 * V12 * (IW12 - 2 * detIW * A12);
		c = 1 + 4 * A12 * (IW12 - detIW * A12);
		p2_roots rootsQ1b = find_roots(a, b, c);
		if (alpha_th < 1)
			sc_out.factor = fmin(a + b + c, 1);
		sc_out.is_SC =
			!rootsQ1a.in_range(0, min(1.0 + dm, alpha_th + dm))
			&& !rootsQ1b.in_range(min(1.0 + dm, alpha_th + dm), 1 + dm)
			&& (sc_out.factor > th);

		return sc_out;
	}
	double b = b0 + 2 * V12 * IW12;
	double c = 1;
	p2_roots rootsQ2b = find_roots(a, b, c);
	sc_out.factor = fmin(a + b + c, 1);
	if (V12 <= 0 || alpha_th > 1 + dm) {
		sc_out.is_SC = !rootsQ2b.in_range(0, 1 + dm)
			&& (sc_out.factor > th);
		return sc_out;
	}
	b = b0 - 2 * V12 * (IW12 - 2 * A12 * detIW);
	c = 1 - 4 * A12 * (IW12 + detIW * A12);
	if (alpha_th > 1)
		sc_out.factor = fmin(a + b + c, 1);
	p2_roots rootsQ2a = find_roots(a, b, c);
	sc_out.is_SC =
		(!rootsQ2a.in_range(min(1.0 + dm, alpha_th + dm), 1 + dm))
		&& (!rootsQ2b.in_range(0, min(1.0 + dm, alpha_th + dm)))
		&& (sc_out.factor > th);
	return sc_out;

}

/// Check if the model is Weak Walk-Summable (WWS) through eigenvalue decompostion
SC_state is_WWS_model()
{
	SC_state returnValue;
	int const n = S.rows();
	MatrixXd R(n, n);

	LLT<MatrixXd> llt(S); // compute the Cholesky decomposition of S
    returnValue.is_SC = llt.info() != Eigen::NumericalIssue;

	// FIXME: can eigen do that for me?
	for (int i = 0; i < n; i++) {
		R(i, i) = 0;
		for (int j = i + 1; j < n; j++) {
			R(i, j) = S(i, j) / sqrt(S(j, j) * S(i, i));
			R(j, i) = S(j, i) / sqrt(S(j, j) * S(i, i));
		}
	}
	SelfAdjointEigenSolver<MatrixXd> sol(R, false); // do not compute eigen vectors
	VectorXd lp = sol.eigenvalues();
	// FIXME: use diretly minCoeff
	double rho = 1;
	for (int i = 0; i < n; i++)
		rho = min(rho, lp(i));

	returnValue.radius = rho;
	if (!returnValue.is_SC)
		cout <<
			"The model is not weak walk-summable (spectral radius  < -1):\t"
		     << rho << endl;
	if (p.VERBOSITY > 1)
		cout << "Real spectral radius: " << rho << endl;
	return returnValue;
}

/// Check if the model remains WWS after adding after perturbation using determinant variation
SC_state is_WWS_after_perturbation(int n1, int n2)
{
	double const dm = 0.1, th = 0.9;
	struct SC_state sc_out = { -1.0, 1.0, false };

	double const detC = C(n1, n1) * C(n2, n2) - C(n1, n2) * C(n2, n1);
	double const detIA = IA(n1, n1) * IA(n2, n2) - IA(n1, n2) * IA(n2, n1);
	double const V11 = C(n2, n2) / detC - IA(n2, n2) / detIA;
	double const V22 = C(n1, n1) / detC - IA(n1, n1) / detIA;
	double const V12 = IA(n1, n2) / detIA - C(n1, n2) / detC;
	double const detV = V11 * V22 - V12 * V12;
	double const IS11 = IS(n1, n1);
	double const IS22 = IS(n2, n2);
	double const IS12 = IS(n1, n2);
	double const detIS = IS11 * IS22 - IS12 * IS12;

	double const a = detV * detIS;
	double const b = (V11 * IS11 + V22 * IS22 - 2 * V12 * IS12);
	double const c = 1;
	p2_roots rootsQ = find_roots(a, b, c);
	sc_out.factor = a + b + c;
	sc_out.is_SC = !rootsQ.in_range(0, 1 + dm) && (sc_out.factor > th);
	return sc_out;
}


/// Check if the model respects spectral constraints using eigenvalue decomposition.
SC_state is_GSC_model()
{
	struct SC_state returnValue = { -1.0, 1.0, true };
	int const n = A.rows();
	//FIXME: only the diagonal or R should be set to 0 (performance)
	MatrixXd R = MatrixXd::Zero(n,n);
	for (int i = 0; i < n; i++) {
		for (int j = i + 1; j < n; j++) {
			R(i, j) = -A(i, j) / sqrt(A(j, j) * A(i, i));
			R(j, i) = -A(j, i) / sqrt(A(j, j) * A(i, i));
		}
	}
	SelfAdjointEigenSolver<MatrixXd> sol(R, false); // do not compute eigen vectors
	VectorXd lp = sol.eigenvalues();

	// FIXME: use minCoeff and maxCoeff
	double rho_min = 1, rho_max = -1;
	for (int i = 0; i < n; i++) {
		rho_min = fmin(rho_min, lp(i));
		rho_max = fmax(rho_max, lp(i));
	}

	if (rho_min < p.spLowBound || rho_max > p.spUpBound)
		returnValue.is_SC = false;

	if (!returnValue.is_SC)
		cout << "The model violate GSC (rho_min/rho_max):\t" << rho_min
		     << "\t" << rho_max << endl;
	if (p.VERBOSITY > 1)
		cout << "Real spectral radius: " << rho_min << "\t" << rho_max
		     << endl;
	return returnValue;
}

/// Check if the model still respect spectral constraints after perturbation using det. variation
SC_state is_GSC_after_perturbation(int n1, int n2)
{
	double const dm = 0, th = 0;
	struct SC_state sc_out = { -1.0, 1.0, false };

	double const detC = C(n1, n1) * C(n2, n2) - C(n1, n2) * C(n2, n1);
	double const detIA = IA(n1, n1) * IA(n2, n2) - IA(n1, n2) * IA(n2, n1);
	double const V11 = C(n2, n2) / detC - IA(n2, n2) / detIA;
	double const V22 = C(n1, n1) / detC - IA(n1, n1) / detIA;
	double const V12 = IA(n1, n2) / detIA - C(n1, n2) / detC;
	double const Vp12 = V12 / p.spUpBound;
	double const Vm12 = V12 / p.spLowBound;
	double const detVp = V11 * V22 - Vp12 * Vp12;
	double const detVm = V11 * V22 - Vm12 * Vm12;
	double const ISp11 = ISp(n1, n1);
	double const ISm11 = ISm(n1, n1);
	double const ISp22 = ISp(n2, n2);
	double const ISm22 = ISm(n2, n2);
	double const ISp12 = ISp(n1, n2);
	double const ISm12 = ISm(n1, n2);
	double const detISp = ISp11 * ISp22 - ISp12 * ISp12;
	double const detISm = ISm11 * ISm22 - ISm12 * ISm12;

	double const ap = detVp * detISp;
	double const am = detVm * detISm;
	double const bp = (V11 * ISp11 + V22 * ISp22 + 2 * Vp12 * ISp12);
	double const bm = (V11 * ISm11 + V22 * ISm22 + 2 * Vm12 * ISm12);
	double const c = 1;

	p2_roots rootsM = find_roots(am, bm, c);
	p2_roots rootsP = find_roots(ap, bp, c);
	sc_out.factor = min(ap + bp + c, am + bm + c);
	sc_out.is_SC = !rootsM.in_range(0, 1 + dm)
		&& !rootsP.in_range(0, 1 + dm) && (sc_out.factor > th);
	return sc_out;
}


// check the correct is_XXX_model depending on selected constraint
bool is_correct_model()
{
	switch (p.sconstraints) {
	case WS:
		return is_WS_model().is_SC;
	case WWS:
		return is_WWS_model().is_SC;
	case GSC:
		return is_GSC_model().is_SC;
	default:
		return true;
	}
}


SC_state estimate_SC(int node1, int node2)
{
	struct SC_state returnValue = { -1.0, 1.0, true };
	switch (p.sconstraints) {
	case WS:
		return is_WS_after_perturbation(node1, node2);
	case WWS:
		return is_WWS_after_perturbation(node1, node2);
	case GSC:
		return is_GSC_after_perturbation(node1, node2);
	case NONE:
		return returnValue;
	default:
		cerr << "error -- spectral constraints unspecified!" << endl;
		returnValue.is_SC = false;
		return returnValue;
	}
}


// Compute the dual bound for the update step.
double get_LL_bound()
{
	double bound = 0;
	for (int n = 0; n < p.Nv; n++) {
		int const nvn = TheModel->nv[n];
		for (int m = n; m < p.Nv; m++) {
			double x = 0;
			if (abs(A(n, m)) > POSITIVE_TH)
				x += A(n, n) * (C(m, n) - IA(m, n));
			for (int j = 0; j < nvn; j++) {
				int const p = TheModel->Vois[n][j];
				if (abs(A(m, p)) > POSITIVE_TH)
					x += A(n, p) * (C(m, p) - IA(m, p));
			}
			if (m == n)
				bound += x * x / 2;
			else
				bound += x * x;
		}
	}
	return bound;
}

// Block update.
double block_update(int i)
{
	int const d = TheModel->nv[i], N = A.rows();
	if (d == 0)
		return 0;
	//FIXME: no need to set B, v and ai to 0 (performance)
	MatrixXd B(d,d), IB;
	VectorXd u, v(d), ai(d), bi;
	for (int j = 0; j < d; j++) {
		int const n = TheModel->Vois[i][j];
		B(j, j) = IA(n, n) - IA(n, i) * IA(i, n) / IA(i, i);
		v(j) = - C(i, n) / C(i, i);
		ai(j) = A(i, n);
		for (int k = j + 1; k < d; k++) {
			int const m = TheModel->Vois[i][k];
			B(j, k) = IA(n, m) - IA(n, i) * IA(m, i) / IA(i, i);
			B(k, j) = B(j, k);
		}
	}
	/*
	  Here we do not use equation (5) from the paper to update A_{ii}
	  because it is incorrect. Indeed the right formula for $A'_{ii}$
	  should depend on $\>A'_i$ and not $\>A_i$, and thus some simplifications ensue.
	  \begin{align*}
	  A'_{ii} &= \frac{1}{\hat C_{ii}}+\>A'_i^T\>A_{\backslash i\backslash i}^{-1} \>A'_i\\
	          &= \frac{1}{\hat C_{ii}}+ \frac{1}{\hat C_{ii}^2}\>{\hat C}_{i}^T\>A_{\backslash i\backslash i}^{-1}\>{\hat C}_{i}^T\Bigl]
	  \end{align*}
	  Notation: the current translations are useful to compare the code and the paper.
	  IA --> C
	  B--> A\i\i^{-1}
	  IB  --> I_vi [A\i\i^{-1}]^{-1}I_vi
	  u --> {\bf A}'_i
	  y --> A'_ii
	 */
	IB = B.inverse();
	u = IB * v;
	double const aii = A(i, i);
	// dot() does a scalar product (aka dot product)
	A(i, i) = 1.0 / C(i, i) + u.dot(v);
	double const uii = A(i,i) - aii;
	bi = B * ai;
	double const ab = ai.dot(bi);
	double dll = -logapprox(C(i, i) * (aii - ab)) - uii * C(i, i);
	for (int j = 0; j < d; j++) {
		int const n = TheModel->Vois[i][j];
		double const x = u(j);
		u(j) = x - A(i, n);
		dll -= 2 * (x - A(i, n)) * C(i, n);
		A(i, n) = x;
		A(n, i) = x;
	}
	double const z = u.squaredNorm() + 1e-20;
	double a = 0;
	for (int j = 0; j < d; j++) {
		int n = TheModel->Vois[i][j];
		a += u(j) * IA(i, n);
	}
	double const b = z * IA(i, i);
	v.setZero(N);
	for (int n = 0; n < N; n++) {
		if (n == i)
			continue;
		double x = 0;
		for (int j = 0; j < d; j++) {
			int m = TheModel->Vois[i][j];
			x += IA(n,m) * u(j);
		}
		v(n) = x / z;
	}
	double g = 0;
	for (int j = 0; j < d; j++) {
		int const m = TheModel->Vois[i][j];
		g += v(m) * u(j);
	}
	double const h = uii / z;
	double const det = pow(1 + a, 2) + b * (h - g);
	Matrix2d Mat;
	Mat << -b,     1 + a,
	        1 + a, h - g;
	Mat *= z / det;
	VectorXd const IAi = IA.col(i);
	// save v and A_i in a 2-column matrix (v is a column vector)
	MatrixX2d vIAi(N,2);
	vIAi.col(0) = v;
	vIAi.col(1) = IAi;
	IA.noalias() -= vIAi * Mat * vIAi.transpose();

	double dx = b * (det - 1) / det / z;
	IA(i, i)  = IAi(i) - dx;
	for (int n = 0; n < N; n++) {
		if (n == i)
			continue;
		double x1 = v(n);
		double y1 = IAi(n);
		dx = (b * x1 + (a * (1 + a) + b * (h - g)) * y1) / det;
		IA(i, n) = IAi(n) - dx;
		IA(n, i) = IA(i, n);
	}

	return dll;
}

///Compute the log-likelihood of M with respect to Ref
double get_LL(const MatrixXd & M, const MatrixXd & Ref)
{
	double const x = logdet(M) - (Ref * M).trace();
	return is_correct_model() ? x : -1e10;
}


bool
loop_creation(int node1, int node2, size_t depth_max, bool sign, bool floop)
{
	///Check if the nodes have a common neighbor looking at the second order neighborhood of one node
	///bool sign is true if the new edge btw node1 and node2 is a repulsive one
	///bool f_loop is true if we only want to remove frustrated loops
	bool bCreate_Loop = false;
	std::queue < int >theQueue;	/// nodes to be investigated
	std::queue < size_t > theDepths;	/// depths of the nodes within the neighborhood of node1
	std::queue < bool > theSigns;	/// indicates wether the path is attractive (false) or repulsive (true)
	theQueue.push(node1);
	theDepths.push(0);
	theSigns.push(false);
	while (!bCreate_Loop && !theQueue.empty()) {	///Will stop at the first loop found or when the queue is empty.
		int current_node = theQueue.front();
		size_t current_depth = theDepths.front();
		if (current_node == node2) {
			if (floop)
				bCreate_Loop = theSigns.front() ^ sign;
			else
				bCreate_Loop = true;
		} else if (current_depth < depth_max) {	///Adding the neighborhood if not too deep.
			for (int v = 0; v < TheModel->nv[current_node]; v++) {
				theQueue.push(TheModel->Vois[current_node][v]);
				theDepths.push(current_depth + 1);
				bool e_sign = A(current_node, TheModel->Vois[current_node][v]) > 0;
				theSigns.push(e_sign ^ theSigns.front());
			}
		}
		theQueue.pop();
		theDepths.pop();
		theSigns.pop();
	}
	return bCreate_Loop;
}


///Compute the potential LL increments.
void set_GLL_weights(int l, bool fixed_size)
{
	int const A_size = (int)A.rows();
	wLL.resize(0, 0);
	wLL.resize(l + 1, 4);
	for (int n = 0; n < A_size; n++) {
		double const bi = IA(n, n);
		double const ci = C(n, n);
		int q;
		if (fixed_size)
			q = TheModel->nv[n];
		else
			q = TheModel->nv0[n];
		for (int j = 0; j < q; j++) {
			int const m = TheModel->Vois[n][j];
			if (m <= n)
				continue;
			double const bj = IA(m, m);
			double const cj = C(m, m);
			double const bij = IA(m, n);
			double const cij = C(m, n);
			double const db = bi * bj - bij * bij;
			double const dc = ci * cj - cij * cij;
			wLL[l][1] = n;
			wLL[l][2] = j;

			///Link addition.
			if (j >= TheModel->nv[n]) {
				wLL[l][0] = (bi * cj + ci * bj - 2 * bij * cij) / db - 2 - logapprox(dc / db);
				wLL[l][3] = 1;

				if (wLL[l][0] > wLL[l - 1][0]) {	///This link could be added (it is in the short list)
					SC_state sc = estimate_SC(m, n);
					if (!sc.is_SC)
						wLL[l][0] = -1;
					if (p.LOOP_SIZE != 0) {
						if (loop_creation(n, m, abs(p.LOOP_SIZE) - 1, (bij / db - cij / dc) > 0, p.LOOP_SIZE < 0)) {
							wLL[l][0] = -1;
							add_edge_to_blacklist(n, j);
							q--;
							j--;
						}
					}
				}
			}
			/// Link update or deletion.
			else {
				double upd = (bi * cj + ci * bj - 2 * bij * cij) / db - 2 - logapprox(dc / db);
				double del = -1;
				double const aij = A(m, n);
				if (1 - 2 * aij * bij - aij * aij * db > 0) {	///Does the model remains def. pos. after link suppression?
					del = logapprox(1 - 2 * aij * bij - aij * aij * db) + 2 * aij * cij;
				}

				if ((upd >= del) && (upd > wLL[l - 1][0])) {	///This link could be updated (it is in the short list)
					SC_state sc = estimate_SC(m, n);
					if (!sc.is_SC)
						upd = -1;
				}

				if (upd > 0 || del > 0) {
					if (p.ADD_PENALTY + del < 0 || fixed_size) {	///choosing between update and deletion.
						wLL[l][0] = upd;	///update.
						wLL[l][3] = 0;
					} else {
						wLL[l][0] = del + p.ADD_PENALTY;	///deletion
						wLL[l][3] = -1;
					}
				}
			}
			int p = l - 1;
			while (p >= 0 && wLL[l][0] > wLL[p][0]) {
				if (p < l - 1)
					for (int a = 0; a < 4; a++)
						wLL[p + 1][a] = wLL[p][a];
				p--;
				if (p == -1)
					continue;
			}
			if (p < l - 1)
				for (int a = 0; a < 4; a++)
					wLL[p + 1][a] = wLL[l][a];
		}
	}
	if (wLL[0][3] == -1)
		wLL[0][0] -= p.ADD_PENALTY;
}
//pre selection of candidate links
void pre_select_links(double r)
{
	int const A_size = (int)A.rows();
	std::vector<double> w(A_size*(A_size-1)/2);
	int l=0;
	for (int n = 0; n < A_size; n++) 
	{
		double const bi = IA(n, n);
		double const ci = C(n, n);
		int q = TheModel->nv0[n];
		for (int j = 0; j < q; j++) 
		{
			int const m = TheModel->Vois[n][j];
			if (m <= n)
				continue;
			double const bj = IA(m, m);
			double const cj = C(m, m);
			double const bij = IA(n, m);
			double const cij = C(n, m);
			double const db = bi * bj - bij * bij;
			double const dc = ci * cj - cij * cij;
			if (j >= TheModel->nv[n]) 
			{
				w[l] = (bi * cj + ci * bj - 2 * bij * cij) / db - 2 - logapprox(dc / db);
				l++;
			}
		}
	}
	sort(w.begin(),w.end());
	double th = w[(int)((1-r)*l)];
	cout << "Selection threshold: " << th << endl;
	l=0;
	for (int n = 0; n < A_size; n++) 
	{
		double const bi = IA(n, n);
		double const ci = C(n, n);
		int q = TheModel->nv0[n];
		for (int j = 0; j < q; j++) 
		{
			int const m = TheModel->Vois[n][j];
			if (m <= n)
				continue;
			double const bj = IA(m, m);
			double const cj = C(m, m);
			double const bij = IA(n, m);
			double const cij = C(n, m);
			double const db = bi * bj - bij * bij;
			double const dc = ci * cj - cij * cij;
			if (j >= TheModel->nv[n]) 
			{
				double x = (bi * cj + ci * bj - 2 * bij * cij) / db - 2 - logapprox(dc / db);
				if (x<th)
				{
					add_edge_to_blacklist(n, j);
					q--;
					j--;
				}
				else
					l++;
			}
		}
	}
	cout << "#links: " << l << "/" << A_size*(A_size-1)/2 << endl;
} 

///Best change.
double add_maxLL_link(bool fixed_connectivity)
{
	std::vector < int >blist;
	int const A_size = (int)A.rows();
	int const l = 1;
	set_GLL_weights(l, fixed_connectivity);
	double score = wLL[0][0];
	if (wLL[l - 1][0] > 0) {
		if (p.VERBOSITY > 1 && !fixed_connectivity) {
			if (wLL[0][3] == 1) {
				cout << "adding " << l << " links..." << flush;
				tll = 100;	//update of decision threshold for add/change for the next move
				score += p.ADD_PENALTY;
			}
			if (wLL[0][3] == 0)
				cout << "changing " << l << " links ..." <<
					flush;
			if (wLL[0][3] == -1) {
				cout << "removing" << l << " links..." << flush;
				score -= p.ADD_PENALTY;
			}
		}

		blist.resize(A.rows());
		for (int i = 0; i < l; i++) {
			double vi, vj, vij;
			int const n = wLL[i][1];
			int const j = wLL[i][2];
			int const m = TheModel->Vois[n][j];
			int const ii = TheModel->ind[n][j];
			double const ai = A(n, n);	//current precision matrix
			double const aj = A(m, m);
			double const aij = A(n, m);
			double const bi = IA(n, n);	//current covariance matrix
			double const bj = IA(m, m);
			double const bij = IA(n, m);
			double const db = bi * bj - bij * bij;
			if (wLL[i][3] >= 0) {	//addition/modification of one link
				double const ci = C(n, n);	//ref covariant matrix
				double const cj = C(m, m);
				double const cij = C(n, m);
				double const dc = ci * cj - cij * cij;
				vi = cj / dc - bj / db;
				vj = ci / dc - bi / db;
				vij = bij / db - cij / dc;
			} else {	//suppression of one link
				vi = 0;
				vj = 0;
				vij = -aij;
			}
			update_matrices(n, m, ai, aj, aij, vi, vj, vij);

			Matrix2d P;	///2x2 Matrix needed for updating of the inverses
			P << vi, vij,
				vij, vj;
			update_inverse(n, m, IA, P); //covariance MatrixXd.
			switch (p.sconstraints) {
			case WS:
				P(0, 1) = -abs(aij + vij) + abs(aij);
				P(1, 0) = -abs(aij + vij) + abs(aij);
				update_inverse(n, m, IW, P);
				break;
			case WWS:
				P(0, 1) = -vij;
				P(1, 0) = -vij;
				update_inverse(n, m, IS, P);
				break;
			case GSC:
				P(0, 1) = vij / p.spUpBound;
				P(1, 0) = vij / p.spUpBound;
				update_inverse(n, m, ISp, P);
				P(0, 1) = vij / p.spLowBound;
				P(1, 0) = vij / p.spLowBound;
				update_inverse(n, m, ISm, P);
				break;
			default:
				break;
			}

			if (j >= TheModel->nv[n]) {	//Link addition
				TheModel->re_index(n, j, TheModel->nv[n]);
				TheModel->nv[n]++;
				TheModel->re_index(m, ii, TheModel->nv[m]);
				TheModel->nv[m]++;
				TheModel->Km += 2.0 / A_size;
			} else if (wLL[i][3] == -1) {	//Link deletion
				TheModel->re_index(n, j, TheModel->nv[n] - 1);
				TheModel->nv[n]--;
				TheModel->re_index(m, ii, TheModel->nv[m] - 1);
				TheModel->nv[m]--;
				TheModel->Km -= 2.0 / A_size;
			}
		}
		if (p.VERBOSITY > 1 && !fixed_connectivity)
			cout << "DONE\n";
	}
	return score;
}
