#! /bin/bash

cd $1

TIME="/usr/bin/time --format=%e"
STARIPS=../../star-ips
CONF=test.config
CHECK_OUT=couplings.dat
OUT="$OUT LL.dat"


# check whether we have test files

# $1 is file name and $2 is description
function checkread {
    [ -r $1 ] || {
	echo Missing $2 file $1
	exit 1
    }
}

checkread $CONF configuration
for i in $CHECK_OUT ; do
    checkread ref-$i reference
done

# run starips

echo -n $(basename `pwd`):" "
$TIME -o time.out $STARIPS -conf $CONF >/dev/null || {
    echo FAILED
    exit 1
}

# check whether output is OK

function checkref {
    cmp --quiet $1 ref-$1 || {
	echo FAILED: Incorrect file $1
	return 1
    }
}

failed=false
for i in $CHECK_OUT ; do
    checkref $i || failed=true
done

if $failed ; then
    :
else
  # Everything is fine, show timing
  echo OK $(cat time.out)
fi
 
rm -f $OUT time.out

if $failed ; then
    exit 1
fi
