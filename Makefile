CXX = g++
OPTFLAGS= -O3 -march=native -ffast-math -DNDEBUG
# This one is for debugging
#OPTFLAGS = -O0 -D_GLIBCXX_DEBUG
CXXFLAGS = -g -std=c++11 -Wall -Wextra $(OPTFLAGS)
CXXLD = $(CXX) $(CXXFLAGS)
LIBS = -lm

TARGET = star-ips

OBJECTS = star-ips.o IPSMethod.o GMRF.o Tools.o

####### Build rules

all: $(TARGET)

star-ips: $(OBJECTS)
	$(CXXLD) -o $@ $^ $(LIBS)

clean:
	-rm -f *.o *~ *# *.orig Makefile.bak $(TARGET)

depend:
	makedepend -Y $(OBJECTS:.o=.cpp) 2>/dev/null

indent:
	astyle --style=linux --indent=tab *.cpp *.h

tests: star-ips
	for dir in tests/*/ ; do ./runtest.sh $$dir ; done

slowtests: star-ips
	for dir in slowtests/*/ ; do ./runtest.sh $$dir ; done

alltests: tests slowtests

archive:
	@if test -z "$(VERSION)" ; then\
	  echo "Please give a version, like \"make archive VERSION=1.0\""; \
	  echo "The version should have been tagged, e.g. \"git tag v1.0\""; \
	  exit 1; \
	fi
	git archive -o star-ips-$(VERSION).tar.gz --prefix=star-ips-$(VERSION)/ v$(VERSION)

$(OBJECTS): Makefile

# DO NOT DELETE THIS LINE -- make depend depends on it.

star-ips.o: GMRF.h Tools.h simd_math_prims.h Eigen/Core
star-ips.o: Eigen/src/Core/util/DisableStupidWarnings.h
star-ips.o: Eigen/src/Core/util/Macros.h Eigen/src/Core/util/MKL_support.h
star-ips.o: Eigen/src/Core/util/Constants.h Eigen/src/Core/util/Meta.h
star-ips.o: Eigen/src/Core/util/ForwardDeclarations.h
star-ips.o: Eigen/src/Core/util/StaticAssert.h
star-ips.o: Eigen/src/Core/util/XprHelper.h Eigen/src/Core/util/Memory.h
star-ips.o: Eigen/src/Core/NumTraits.h Eigen/src/Core/MathFunctions.h
star-ips.o: Eigen/src/Core/GenericPacketMath.h
star-ips.o: Eigen/src/Core/MathFunctionsImpl.h
star-ips.o: Eigen/src/Core/arch/Default/ConjHelper.h
star-ips.o: Eigen/src/Core/arch/CUDA/Half.h
star-ips.o: Eigen/src/Core/arch/CUDA/PacketMathHalf.h
star-ips.o: Eigen/src/Core/arch/CUDA/TypeCasting.h
star-ips.o: Eigen/src/Core/arch/Default/Settings.h
star-ips.o: Eigen/src/Core/functors/TernaryFunctors.h
star-ips.o: Eigen/src/Core/functors/BinaryFunctors.h
star-ips.o: Eigen/src/Core/functors/UnaryFunctors.h
star-ips.o: Eigen/src/Core/functors/NullaryFunctors.h
star-ips.o: Eigen/src/Core/functors/StlFunctors.h
star-ips.o: Eigen/src/Core/functors/AssignmentFunctors.h
star-ips.o: Eigen/src/Core/arch/CUDA/Complex.h Eigen/src/Core/IO.h
star-ips.o: Eigen/src/Core/DenseCoeffsBase.h Eigen/src/Core/DenseBase.h
star-ips.o: Eigen/src/plugins/BlockMethods.h Eigen/src/Core/MatrixBase.h
star-ips.o: Eigen/src/plugins/CommonCwiseUnaryOps.h
star-ips.o: Eigen/src/plugins/CommonCwiseBinaryOps.h
star-ips.o: Eigen/src/plugins/MatrixCwiseUnaryOps.h
star-ips.o: Eigen/src/plugins/MatrixCwiseBinaryOps.h
star-ips.o: Eigen/src/Core/EigenBase.h Eigen/src/Core/Product.h
star-ips.o: Eigen/src/Core/CoreEvaluators.h Eigen/src/Core/AssignEvaluator.h
star-ips.o: Eigen/src/Core/Assign.h Eigen/src/Core/ArrayBase.h
star-ips.o: Eigen/src/plugins/ArrayCwiseUnaryOps.h
star-ips.o: Eigen/src/plugins/ArrayCwiseBinaryOps.h
star-ips.o: Eigen/src/Core/util/BlasUtil.h Eigen/src/Core/DenseStorage.h
star-ips.o: Eigen/src/Core/NestByValue.h Eigen/src/Core/ReturnByValue.h
star-ips.o: Eigen/src/Core/NoAlias.h Eigen/src/Core/PlainObjectBase.h
star-ips.o: Eigen/src/Core/Matrix.h Eigen/src/Core/Array.h
star-ips.o: Eigen/src/Core/CwiseTernaryOp.h Eigen/src/Core/CwiseBinaryOp.h
star-ips.o: Eigen/src/Core/CwiseUnaryOp.h Eigen/src/Core/CwiseNullaryOp.h
star-ips.o: Eigen/src/Core/CwiseUnaryView.h
star-ips.o: Eigen/src/Core/SelfCwiseBinaryOp.h Eigen/src/Core/Dot.h
star-ips.o: Eigen/src/Core/StableNorm.h Eigen/src/Core/Stride.h
star-ips.o: Eigen/src/Core/MapBase.h Eigen/src/Core/Map.h
star-ips.o: Eigen/src/Core/Ref.h Eigen/src/Core/Block.h
star-ips.o: Eigen/src/Core/VectorBlock.h Eigen/src/Core/Transpose.h
star-ips.o: Eigen/src/Core/DiagonalMatrix.h Eigen/src/Core/Diagonal.h
star-ips.o: Eigen/src/Core/DiagonalProduct.h Eigen/src/Core/Redux.h
star-ips.o: Eigen/src/Core/Visitor.h Eigen/src/Core/Fuzzy.h
star-ips.o: Eigen/src/Core/Swap.h Eigen/src/Core/CommaInitializer.h
star-ips.o: Eigen/src/Core/GeneralProduct.h Eigen/src/Core/Solve.h
star-ips.o: Eigen/src/Core/Inverse.h Eigen/src/Core/SolverBase.h
star-ips.o: Eigen/src/Core/PermutationMatrix.h
star-ips.o: Eigen/src/Core/Transpositions.h Eigen/src/Core/TriangularMatrix.h
star-ips.o: Eigen/src/Core/SelfAdjointView.h
star-ips.o: Eigen/src/Core/products/GeneralBlockPanelKernel.h
star-ips.o: Eigen/src/Core/products/Parallelizer.h
star-ips.o: Eigen/src/Core/ProductEvaluators.h
star-ips.o: Eigen/src/Core/products/GeneralMatrixVector.h
star-ips.o: Eigen/src/Core/products/GeneralMatrixMatrix.h
star-ips.o: Eigen/src/Core/SolveTriangular.h
star-ips.o: Eigen/src/Core/products/GeneralMatrixMatrixTriangular.h
star-ips.o: Eigen/src/Core/products/SelfadjointMatrixVector.h
star-ips.o: Eigen/src/Core/products/SelfadjointMatrixMatrix.h
star-ips.o: Eigen/src/Core/products/SelfadjointProduct.h
star-ips.o: Eigen/src/Core/products/SelfadjointRank2Update.h
star-ips.o: Eigen/src/Core/products/TriangularMatrixVector.h
star-ips.o: Eigen/src/Core/products/TriangularMatrixMatrix.h
star-ips.o: Eigen/src/Core/products/TriangularSolverMatrix.h
star-ips.o: Eigen/src/Core/products/TriangularSolverVector.h
star-ips.o: Eigen/src/Core/BandMatrix.h Eigen/src/Core/CoreIterators.h
star-ips.o: Eigen/src/Core/ConditionEstimator.h Eigen/src/Core/BooleanRedux.h
star-ips.o: Eigen/src/Core/Select.h Eigen/src/Core/VectorwiseOp.h
star-ips.o: Eigen/src/Core/Random.h Eigen/src/Core/Replicate.h
star-ips.o: Eigen/src/Core/Reverse.h Eigen/src/Core/ArrayWrapper.h
star-ips.o: Eigen/src/Core/GlobalFunctions.h
star-ips.o: Eigen/src/Core/util/ReenableStupidWarnings.h IPSMethod.h
star-ips.o: Parameters.h Parameters-def.h Eigen/Eigenvalues Eigen/Core
star-ips.o: Eigen/Cholesky Eigen/Jacobi Eigen/src/Jacobi/Jacobi.h
star-ips.o: Eigen/src/Cholesky/LLT.h Eigen/src/Cholesky/LDLT.h
star-ips.o: Eigen/Householder Eigen/src/Householder/Householder.h
star-ips.o: Eigen/src/Householder/HouseholderSequence.h
star-ips.o: Eigen/src/Householder/BlockHouseholder.h Eigen/LU
star-ips.o: Eigen/src/misc/Kernel.h Eigen/src/misc/Image.h
star-ips.o: Eigen/src/LU/FullPivLU.h Eigen/src/LU/PartialPivLU.h
star-ips.o: Eigen/src/LU/Determinant.h Eigen/src/LU/InverseImpl.h
star-ips.o: Eigen/Geometry Eigen/SVD Eigen/QR Eigen/src/QR/HouseholderQR.h
star-ips.o: Eigen/src/QR/FullPivHouseholderQR.h
star-ips.o: Eigen/src/QR/ColPivHouseholderQR.h
star-ips.o: Eigen/src/QR/CompleteOrthogonalDecomposition.h
star-ips.o: Eigen/src/misc/RealSvd2x2.h
star-ips.o: Eigen/src/SVD/UpperBidiagonalization.h Eigen/src/SVD/SVDBase.h
star-ips.o: Eigen/src/SVD/JacobiSVD.h Eigen/src/SVD/BDCSVD.h
star-ips.o: Eigen/src/Geometry/OrthoMethods.h
star-ips.o: Eigen/src/Geometry/EulerAngles.h Eigen/src/Geometry/Homogeneous.h
star-ips.o: Eigen/src/Geometry/RotationBase.h Eigen/src/Geometry/Rotation2D.h
star-ips.o: Eigen/src/Geometry/Quaternion.h Eigen/src/Geometry/AngleAxis.h
star-ips.o: Eigen/src/Geometry/Transform.h Eigen/src/Geometry/Translation.h
star-ips.o: Eigen/src/Geometry/Scaling.h Eigen/src/Geometry/Hyperplane.h
star-ips.o: Eigen/src/Geometry/ParametrizedLine.h
star-ips.o: Eigen/src/Geometry/AlignedBox.h Eigen/src/Geometry/Umeyama.h
star-ips.o: Eigen/src/Eigenvalues/Tridiagonalization.h
star-ips.o: Eigen/src/Eigenvalues/RealSchur.h
star-ips.o: Eigen/src/Eigenvalues/./HessenbergDecomposition.h
star-ips.o: Eigen/src/Eigenvalues/EigenSolver.h
star-ips.o: Eigen/src/Eigenvalues/./RealSchur.h
star-ips.o: Eigen/src/Eigenvalues/SelfAdjointEigenSolver.h
star-ips.o: Eigen/src/Eigenvalues/./Tridiagonalization.h
star-ips.o: Eigen/src/Eigenvalues/GeneralizedSelfAdjointEigenSolver.h
star-ips.o: Eigen/src/Eigenvalues/HessenbergDecomposition.h
star-ips.o: Eigen/src/Eigenvalues/ComplexSchur.h
star-ips.o: Eigen/src/Eigenvalues/ComplexEigenSolver.h
star-ips.o: Eigen/src/Eigenvalues/./ComplexSchur.h
star-ips.o: Eigen/src/Eigenvalues/RealQZ.h
star-ips.o: Eigen/src/Eigenvalues/GeneralizedEigenSolver.h
star-ips.o: Eigen/src/Eigenvalues/./RealQZ.h
star-ips.o: Eigen/src/Eigenvalues/MatrixBaseEigenvalues.h
IPSMethod.o: IPSMethod.h Tools.h simd_math_prims.h Eigen/Core
IPSMethod.o: Eigen/src/Core/util/DisableStupidWarnings.h
IPSMethod.o: Eigen/src/Core/util/Macros.h Eigen/src/Core/util/MKL_support.h
IPSMethod.o: Eigen/src/Core/util/Constants.h Eigen/src/Core/util/Meta.h
IPSMethod.o: Eigen/src/Core/util/ForwardDeclarations.h
IPSMethod.o: Eigen/src/Core/util/StaticAssert.h
IPSMethod.o: Eigen/src/Core/util/XprHelper.h Eigen/src/Core/util/Memory.h
IPSMethod.o: Eigen/src/Core/NumTraits.h Eigen/src/Core/MathFunctions.h
IPSMethod.o: Eigen/src/Core/GenericPacketMath.h
IPSMethod.o: Eigen/src/Core/MathFunctionsImpl.h
IPSMethod.o: Eigen/src/Core/arch/Default/ConjHelper.h
IPSMethod.o: Eigen/src/Core/arch/CUDA/Half.h
IPSMethod.o: Eigen/src/Core/arch/CUDA/PacketMathHalf.h
IPSMethod.o: Eigen/src/Core/arch/CUDA/TypeCasting.h
IPSMethod.o: Eigen/src/Core/arch/Default/Settings.h
IPSMethod.o: Eigen/src/Core/functors/TernaryFunctors.h
IPSMethod.o: Eigen/src/Core/functors/BinaryFunctors.h
IPSMethod.o: Eigen/src/Core/functors/UnaryFunctors.h
IPSMethod.o: Eigen/src/Core/functors/NullaryFunctors.h
IPSMethod.o: Eigen/src/Core/functors/StlFunctors.h
IPSMethod.o: Eigen/src/Core/functors/AssignmentFunctors.h
IPSMethod.o: Eigen/src/Core/arch/CUDA/Complex.h Eigen/src/Core/IO.h
IPSMethod.o: Eigen/src/Core/DenseCoeffsBase.h Eigen/src/Core/DenseBase.h
IPSMethod.o: Eigen/src/plugins/BlockMethods.h Eigen/src/Core/MatrixBase.h
IPSMethod.o: Eigen/src/plugins/CommonCwiseUnaryOps.h
IPSMethod.o: Eigen/src/plugins/CommonCwiseBinaryOps.h
IPSMethod.o: Eigen/src/plugins/MatrixCwiseUnaryOps.h
IPSMethod.o: Eigen/src/plugins/MatrixCwiseBinaryOps.h
IPSMethod.o: Eigen/src/Core/EigenBase.h Eigen/src/Core/Product.h
IPSMethod.o: Eigen/src/Core/CoreEvaluators.h Eigen/src/Core/AssignEvaluator.h
IPSMethod.o: Eigen/src/Core/Assign.h Eigen/src/Core/ArrayBase.h
IPSMethod.o: Eigen/src/plugins/ArrayCwiseUnaryOps.h
IPSMethod.o: Eigen/src/plugins/ArrayCwiseBinaryOps.h
IPSMethod.o: Eigen/src/Core/util/BlasUtil.h Eigen/src/Core/DenseStorage.h
IPSMethod.o: Eigen/src/Core/NestByValue.h Eigen/src/Core/ReturnByValue.h
IPSMethod.o: Eigen/src/Core/NoAlias.h Eigen/src/Core/PlainObjectBase.h
IPSMethod.o: Eigen/src/Core/Matrix.h Eigen/src/Core/Array.h
IPSMethod.o: Eigen/src/Core/CwiseTernaryOp.h Eigen/src/Core/CwiseBinaryOp.h
IPSMethod.o: Eigen/src/Core/CwiseUnaryOp.h Eigen/src/Core/CwiseNullaryOp.h
IPSMethod.o: Eigen/src/Core/CwiseUnaryView.h
IPSMethod.o: Eigen/src/Core/SelfCwiseBinaryOp.h Eigen/src/Core/Dot.h
IPSMethod.o: Eigen/src/Core/StableNorm.h Eigen/src/Core/Stride.h
IPSMethod.o: Eigen/src/Core/MapBase.h Eigen/src/Core/Map.h
IPSMethod.o: Eigen/src/Core/Ref.h Eigen/src/Core/Block.h
IPSMethod.o: Eigen/src/Core/VectorBlock.h Eigen/src/Core/Transpose.h
IPSMethod.o: Eigen/src/Core/DiagonalMatrix.h Eigen/src/Core/Diagonal.h
IPSMethod.o: Eigen/src/Core/DiagonalProduct.h Eigen/src/Core/Redux.h
IPSMethod.o: Eigen/src/Core/Visitor.h Eigen/src/Core/Fuzzy.h
IPSMethod.o: Eigen/src/Core/Swap.h Eigen/src/Core/CommaInitializer.h
IPSMethod.o: Eigen/src/Core/GeneralProduct.h Eigen/src/Core/Solve.h
IPSMethod.o: Eigen/src/Core/Inverse.h Eigen/src/Core/SolverBase.h
IPSMethod.o: Eigen/src/Core/PermutationMatrix.h
IPSMethod.o: Eigen/src/Core/Transpositions.h
IPSMethod.o: Eigen/src/Core/TriangularMatrix.h
IPSMethod.o: Eigen/src/Core/SelfAdjointView.h
IPSMethod.o: Eigen/src/Core/products/GeneralBlockPanelKernel.h
IPSMethod.o: Eigen/src/Core/products/Parallelizer.h
IPSMethod.o: Eigen/src/Core/ProductEvaluators.h
IPSMethod.o: Eigen/src/Core/products/GeneralMatrixVector.h
IPSMethod.o: Eigen/src/Core/products/GeneralMatrixMatrix.h
IPSMethod.o: Eigen/src/Core/SolveTriangular.h
IPSMethod.o: Eigen/src/Core/products/GeneralMatrixMatrixTriangular.h
IPSMethod.o: Eigen/src/Core/products/SelfadjointMatrixVector.h
IPSMethod.o: Eigen/src/Core/products/SelfadjointMatrixMatrix.h
IPSMethod.o: Eigen/src/Core/products/SelfadjointProduct.h
IPSMethod.o: Eigen/src/Core/products/SelfadjointRank2Update.h
IPSMethod.o: Eigen/src/Core/products/TriangularMatrixVector.h
IPSMethod.o: Eigen/src/Core/products/TriangularMatrixMatrix.h
IPSMethod.o: Eigen/src/Core/products/TriangularSolverMatrix.h
IPSMethod.o: Eigen/src/Core/products/TriangularSolverVector.h
IPSMethod.o: Eigen/src/Core/BandMatrix.h Eigen/src/Core/CoreIterators.h
IPSMethod.o: Eigen/src/Core/ConditionEstimator.h
IPSMethod.o: Eigen/src/Core/BooleanRedux.h Eigen/src/Core/Select.h
IPSMethod.o: Eigen/src/Core/VectorwiseOp.h Eigen/src/Core/Random.h
IPSMethod.o: Eigen/src/Core/Replicate.h Eigen/src/Core/Reverse.h
IPSMethod.o: Eigen/src/Core/ArrayWrapper.h Eigen/src/Core/GlobalFunctions.h
IPSMethod.o: Eigen/src/Core/util/ReenableStupidWarnings.h GMRF.h Parameters.h
IPSMethod.o: Parameters-def.h Eigen/Eigenvalues Eigen/Core Eigen/Cholesky
IPSMethod.o: Eigen/Jacobi Eigen/src/Jacobi/Jacobi.h Eigen/src/Cholesky/LLT.h
IPSMethod.o: Eigen/src/Cholesky/LDLT.h Eigen/Householder
IPSMethod.o: Eigen/src/Householder/Householder.h
IPSMethod.o: Eigen/src/Householder/HouseholderSequence.h
IPSMethod.o: Eigen/src/Householder/BlockHouseholder.h Eigen/LU
IPSMethod.o: Eigen/src/misc/Kernel.h Eigen/src/misc/Image.h
IPSMethod.o: Eigen/src/LU/FullPivLU.h Eigen/src/LU/PartialPivLU.h
IPSMethod.o: Eigen/src/LU/Determinant.h Eigen/src/LU/InverseImpl.h
IPSMethod.o: Eigen/Geometry Eigen/SVD Eigen/QR Eigen/src/QR/HouseholderQR.h
IPSMethod.o: Eigen/src/QR/FullPivHouseholderQR.h
IPSMethod.o: Eigen/src/QR/ColPivHouseholderQR.h
IPSMethod.o: Eigen/src/QR/CompleteOrthogonalDecomposition.h
IPSMethod.o: Eigen/src/misc/RealSvd2x2.h
IPSMethod.o: Eigen/src/SVD/UpperBidiagonalization.h Eigen/src/SVD/SVDBase.h
IPSMethod.o: Eigen/src/SVD/JacobiSVD.h Eigen/src/SVD/BDCSVD.h
IPSMethod.o: Eigen/src/Geometry/OrthoMethods.h
IPSMethod.o: Eigen/src/Geometry/EulerAngles.h
IPSMethod.o: Eigen/src/Geometry/Homogeneous.h
IPSMethod.o: Eigen/src/Geometry/RotationBase.h
IPSMethod.o: Eigen/src/Geometry/Rotation2D.h Eigen/src/Geometry/Quaternion.h
IPSMethod.o: Eigen/src/Geometry/AngleAxis.h Eigen/src/Geometry/Transform.h
IPSMethod.o: Eigen/src/Geometry/Translation.h Eigen/src/Geometry/Scaling.h
IPSMethod.o: Eigen/src/Geometry/Hyperplane.h
IPSMethod.o: Eigen/src/Geometry/ParametrizedLine.h
IPSMethod.o: Eigen/src/Geometry/AlignedBox.h Eigen/src/Geometry/Umeyama.h
IPSMethod.o: Eigen/src/Eigenvalues/Tridiagonalization.h
IPSMethod.o: Eigen/src/Eigenvalues/RealSchur.h
IPSMethod.o: Eigen/src/Eigenvalues/./HessenbergDecomposition.h
IPSMethod.o: Eigen/src/Eigenvalues/EigenSolver.h
IPSMethod.o: Eigen/src/Eigenvalues/./RealSchur.h
IPSMethod.o: Eigen/src/Eigenvalues/SelfAdjointEigenSolver.h
IPSMethod.o: Eigen/src/Eigenvalues/./Tridiagonalization.h
IPSMethod.o: Eigen/src/Eigenvalues/GeneralizedSelfAdjointEigenSolver.h
IPSMethod.o: Eigen/src/Eigenvalues/HessenbergDecomposition.h
IPSMethod.o: Eigen/src/Eigenvalues/ComplexSchur.h
IPSMethod.o: Eigen/src/Eigenvalues/ComplexEigenSolver.h
IPSMethod.o: Eigen/src/Eigenvalues/./ComplexSchur.h
IPSMethod.o: Eigen/src/Eigenvalues/RealQZ.h
IPSMethod.o: Eigen/src/Eigenvalues/GeneralizedEigenSolver.h
IPSMethod.o: Eigen/src/Eigenvalues/./RealQZ.h
IPSMethod.o: Eigen/src/Eigenvalues/MatrixBaseEigenvalues.h
GMRF.o: GMRF.h Tools.h simd_math_prims.h Eigen/Core
GMRF.o: Eigen/src/Core/util/DisableStupidWarnings.h
GMRF.o: Eigen/src/Core/util/Macros.h Eigen/src/Core/util/MKL_support.h
GMRF.o: Eigen/src/Core/util/Constants.h Eigen/src/Core/util/Meta.h
GMRF.o: Eigen/src/Core/util/ForwardDeclarations.h
GMRF.o: Eigen/src/Core/util/StaticAssert.h Eigen/src/Core/util/XprHelper.h
GMRF.o: Eigen/src/Core/util/Memory.h Eigen/src/Core/NumTraits.h
GMRF.o: Eigen/src/Core/MathFunctions.h Eigen/src/Core/GenericPacketMath.h
GMRF.o: Eigen/src/Core/MathFunctionsImpl.h
GMRF.o: Eigen/src/Core/arch/Default/ConjHelper.h
GMRF.o: Eigen/src/Core/arch/CUDA/Half.h
GMRF.o: Eigen/src/Core/arch/CUDA/PacketMathHalf.h
GMRF.o: Eigen/src/Core/arch/CUDA/TypeCasting.h
GMRF.o: Eigen/src/Core/arch/Default/Settings.h
GMRF.o: Eigen/src/Core/functors/TernaryFunctors.h
GMRF.o: Eigen/src/Core/functors/BinaryFunctors.h
GMRF.o: Eigen/src/Core/functors/UnaryFunctors.h
GMRF.o: Eigen/src/Core/functors/NullaryFunctors.h
GMRF.o: Eigen/src/Core/functors/StlFunctors.h
GMRF.o: Eigen/src/Core/functors/AssignmentFunctors.h
GMRF.o: Eigen/src/Core/arch/CUDA/Complex.h Eigen/src/Core/IO.h
GMRF.o: Eigen/src/Core/DenseCoeffsBase.h Eigen/src/Core/DenseBase.h
GMRF.o: Eigen/src/plugins/BlockMethods.h Eigen/src/Core/MatrixBase.h
GMRF.o: Eigen/src/plugins/CommonCwiseUnaryOps.h
GMRF.o: Eigen/src/plugins/CommonCwiseBinaryOps.h
GMRF.o: Eigen/src/plugins/MatrixCwiseUnaryOps.h
GMRF.o: Eigen/src/plugins/MatrixCwiseBinaryOps.h Eigen/src/Core/EigenBase.h
GMRF.o: Eigen/src/Core/Product.h Eigen/src/Core/CoreEvaluators.h
GMRF.o: Eigen/src/Core/AssignEvaluator.h Eigen/src/Core/Assign.h
GMRF.o: Eigen/src/Core/ArrayBase.h Eigen/src/plugins/ArrayCwiseUnaryOps.h
GMRF.o: Eigen/src/plugins/ArrayCwiseBinaryOps.h
GMRF.o: Eigen/src/Core/util/BlasUtil.h Eigen/src/Core/DenseStorage.h
GMRF.o: Eigen/src/Core/NestByValue.h Eigen/src/Core/ReturnByValue.h
GMRF.o: Eigen/src/Core/NoAlias.h Eigen/src/Core/PlainObjectBase.h
GMRF.o: Eigen/src/Core/Matrix.h Eigen/src/Core/Array.h
GMRF.o: Eigen/src/Core/CwiseTernaryOp.h Eigen/src/Core/CwiseBinaryOp.h
GMRF.o: Eigen/src/Core/CwiseUnaryOp.h Eigen/src/Core/CwiseNullaryOp.h
GMRF.o: Eigen/src/Core/CwiseUnaryView.h Eigen/src/Core/SelfCwiseBinaryOp.h
GMRF.o: Eigen/src/Core/Dot.h Eigen/src/Core/StableNorm.h
GMRF.o: Eigen/src/Core/Stride.h Eigen/src/Core/MapBase.h Eigen/src/Core/Map.h
GMRF.o: Eigen/src/Core/Ref.h Eigen/src/Core/Block.h
GMRF.o: Eigen/src/Core/VectorBlock.h Eigen/src/Core/Transpose.h
GMRF.o: Eigen/src/Core/DiagonalMatrix.h Eigen/src/Core/Diagonal.h
GMRF.o: Eigen/src/Core/DiagonalProduct.h Eigen/src/Core/Redux.h
GMRF.o: Eigen/src/Core/Visitor.h Eigen/src/Core/Fuzzy.h Eigen/src/Core/Swap.h
GMRF.o: Eigen/src/Core/CommaInitializer.h Eigen/src/Core/GeneralProduct.h
GMRF.o: Eigen/src/Core/Solve.h Eigen/src/Core/Inverse.h
GMRF.o: Eigen/src/Core/SolverBase.h Eigen/src/Core/PermutationMatrix.h
GMRF.o: Eigen/src/Core/Transpositions.h Eigen/src/Core/TriangularMatrix.h
GMRF.o: Eigen/src/Core/SelfAdjointView.h
GMRF.o: Eigen/src/Core/products/GeneralBlockPanelKernel.h
GMRF.o: Eigen/src/Core/products/Parallelizer.h
GMRF.o: Eigen/src/Core/ProductEvaluators.h
GMRF.o: Eigen/src/Core/products/GeneralMatrixVector.h
GMRF.o: Eigen/src/Core/products/GeneralMatrixMatrix.h
GMRF.o: Eigen/src/Core/SolveTriangular.h
GMRF.o: Eigen/src/Core/products/GeneralMatrixMatrixTriangular.h
GMRF.o: Eigen/src/Core/products/SelfadjointMatrixVector.h
GMRF.o: Eigen/src/Core/products/SelfadjointMatrixMatrix.h
GMRF.o: Eigen/src/Core/products/SelfadjointProduct.h
GMRF.o: Eigen/src/Core/products/SelfadjointRank2Update.h
GMRF.o: Eigen/src/Core/products/TriangularMatrixVector.h
GMRF.o: Eigen/src/Core/products/TriangularMatrixMatrix.h
GMRF.o: Eigen/src/Core/products/TriangularSolverMatrix.h
GMRF.o: Eigen/src/Core/products/TriangularSolverVector.h
GMRF.o: Eigen/src/Core/BandMatrix.h Eigen/src/Core/CoreIterators.h
GMRF.o: Eigen/src/Core/ConditionEstimator.h Eigen/src/Core/BooleanRedux.h
GMRF.o: Eigen/src/Core/Select.h Eigen/src/Core/VectorwiseOp.h
GMRF.o: Eigen/src/Core/Random.h Eigen/src/Core/Replicate.h
GMRF.o: Eigen/src/Core/Reverse.h Eigen/src/Core/ArrayWrapper.h
GMRF.o: Eigen/src/Core/GlobalFunctions.h
GMRF.o: Eigen/src/Core/util/ReenableStupidWarnings.h Parameters.h
GMRF.o: Parameters-def.h
Tools.o: Tools.h simd_math_prims.h Eigen/Core
Tools.o: Eigen/src/Core/util/DisableStupidWarnings.h
Tools.o: Eigen/src/Core/util/Macros.h Eigen/src/Core/util/MKL_support.h
Tools.o: Eigen/src/Core/util/Constants.h Eigen/src/Core/util/Meta.h
Tools.o: Eigen/src/Core/util/ForwardDeclarations.h
Tools.o: Eigen/src/Core/util/StaticAssert.h Eigen/src/Core/util/XprHelper.h
Tools.o: Eigen/src/Core/util/Memory.h Eigen/src/Core/NumTraits.h
Tools.o: Eigen/src/Core/MathFunctions.h Eigen/src/Core/GenericPacketMath.h
Tools.o: Eigen/src/Core/MathFunctionsImpl.h
Tools.o: Eigen/src/Core/arch/Default/ConjHelper.h
Tools.o: Eigen/src/Core/arch/CUDA/Half.h
Tools.o: Eigen/src/Core/arch/CUDA/PacketMathHalf.h
Tools.o: Eigen/src/Core/arch/CUDA/TypeCasting.h
Tools.o: Eigen/src/Core/arch/Default/Settings.h
Tools.o: Eigen/src/Core/functors/TernaryFunctors.h
Tools.o: Eigen/src/Core/functors/BinaryFunctors.h
Tools.o: Eigen/src/Core/functors/UnaryFunctors.h
Tools.o: Eigen/src/Core/functors/NullaryFunctors.h
Tools.o: Eigen/src/Core/functors/StlFunctors.h
Tools.o: Eigen/src/Core/functors/AssignmentFunctors.h
Tools.o: Eigen/src/Core/arch/CUDA/Complex.h Eigen/src/Core/IO.h
Tools.o: Eigen/src/Core/DenseCoeffsBase.h Eigen/src/Core/DenseBase.h
Tools.o: Eigen/src/plugins/BlockMethods.h Eigen/src/Core/MatrixBase.h
Tools.o: Eigen/src/plugins/CommonCwiseUnaryOps.h
Tools.o: Eigen/src/plugins/CommonCwiseBinaryOps.h
Tools.o: Eigen/src/plugins/MatrixCwiseUnaryOps.h
Tools.o: Eigen/src/plugins/MatrixCwiseBinaryOps.h Eigen/src/Core/EigenBase.h
Tools.o: Eigen/src/Core/Product.h Eigen/src/Core/CoreEvaluators.h
Tools.o: Eigen/src/Core/AssignEvaluator.h Eigen/src/Core/Assign.h
Tools.o: Eigen/src/Core/ArrayBase.h Eigen/src/plugins/ArrayCwiseUnaryOps.h
Tools.o: Eigen/src/plugins/ArrayCwiseBinaryOps.h
Tools.o: Eigen/src/Core/util/BlasUtil.h Eigen/src/Core/DenseStorage.h
Tools.o: Eigen/src/Core/NestByValue.h Eigen/src/Core/ReturnByValue.h
Tools.o: Eigen/src/Core/NoAlias.h Eigen/src/Core/PlainObjectBase.h
Tools.o: Eigen/src/Core/Matrix.h Eigen/src/Core/Array.h
Tools.o: Eigen/src/Core/CwiseTernaryOp.h Eigen/src/Core/CwiseBinaryOp.h
Tools.o: Eigen/src/Core/CwiseUnaryOp.h Eigen/src/Core/CwiseNullaryOp.h
Tools.o: Eigen/src/Core/CwiseUnaryView.h Eigen/src/Core/SelfCwiseBinaryOp.h
Tools.o: Eigen/src/Core/Dot.h Eigen/src/Core/StableNorm.h
Tools.o: Eigen/src/Core/Stride.h Eigen/src/Core/MapBase.h
Tools.o: Eigen/src/Core/Map.h Eigen/src/Core/Ref.h Eigen/src/Core/Block.h
Tools.o: Eigen/src/Core/VectorBlock.h Eigen/src/Core/Transpose.h
Tools.o: Eigen/src/Core/DiagonalMatrix.h Eigen/src/Core/Diagonal.h
Tools.o: Eigen/src/Core/DiagonalProduct.h Eigen/src/Core/Redux.h
Tools.o: Eigen/src/Core/Visitor.h Eigen/src/Core/Fuzzy.h
Tools.o: Eigen/src/Core/Swap.h Eigen/src/Core/CommaInitializer.h
Tools.o: Eigen/src/Core/GeneralProduct.h Eigen/src/Core/Solve.h
Tools.o: Eigen/src/Core/Inverse.h Eigen/src/Core/SolverBase.h
Tools.o: Eigen/src/Core/PermutationMatrix.h Eigen/src/Core/Transpositions.h
Tools.o: Eigen/src/Core/TriangularMatrix.h Eigen/src/Core/SelfAdjointView.h
Tools.o: Eigen/src/Core/products/GeneralBlockPanelKernel.h
Tools.o: Eigen/src/Core/products/Parallelizer.h
Tools.o: Eigen/src/Core/ProductEvaluators.h
Tools.o: Eigen/src/Core/products/GeneralMatrixVector.h
Tools.o: Eigen/src/Core/products/GeneralMatrixMatrix.h
Tools.o: Eigen/src/Core/SolveTriangular.h
Tools.o: Eigen/src/Core/products/GeneralMatrixMatrixTriangular.h
Tools.o: Eigen/src/Core/products/SelfadjointMatrixVector.h
Tools.o: Eigen/src/Core/products/SelfadjointMatrixMatrix.h
Tools.o: Eigen/src/Core/products/SelfadjointProduct.h
Tools.o: Eigen/src/Core/products/SelfadjointRank2Update.h
Tools.o: Eigen/src/Core/products/TriangularMatrixVector.h
Tools.o: Eigen/src/Core/products/TriangularMatrixMatrix.h
Tools.o: Eigen/src/Core/products/TriangularSolverMatrix.h
Tools.o: Eigen/src/Core/products/TriangularSolverVector.h
Tools.o: Eigen/src/Core/BandMatrix.h Eigen/src/Core/CoreIterators.h
Tools.o: Eigen/src/Core/ConditionEstimator.h Eigen/src/Core/BooleanRedux.h
Tools.o: Eigen/src/Core/Select.h Eigen/src/Core/VectorwiseOp.h
Tools.o: Eigen/src/Core/Random.h Eigen/src/Core/Replicate.h
Tools.o: Eigen/src/Core/Reverse.h Eigen/src/Core/ArrayWrapper.h
Tools.o: Eigen/src/Core/GlobalFunctions.h
Tools.o: Eigen/src/Core/util/ReenableStupidWarnings.h Eigen/Eigenvalues
Tools.o: Eigen/Core Eigen/Cholesky Eigen/Jacobi Eigen/src/Jacobi/Jacobi.h
Tools.o: Eigen/src/Cholesky/LLT.h Eigen/src/Cholesky/LDLT.h Eigen/Householder
Tools.o: Eigen/src/Householder/Householder.h
Tools.o: Eigen/src/Householder/HouseholderSequence.h
Tools.o: Eigen/src/Householder/BlockHouseholder.h Eigen/LU
Tools.o: Eigen/src/misc/Kernel.h Eigen/src/misc/Image.h
Tools.o: Eigen/src/LU/FullPivLU.h Eigen/src/LU/PartialPivLU.h
Tools.o: Eigen/src/LU/Determinant.h Eigen/src/LU/InverseImpl.h Eigen/Geometry
Tools.o: Eigen/SVD Eigen/QR Eigen/src/QR/HouseholderQR.h
Tools.o: Eigen/src/QR/FullPivHouseholderQR.h
Tools.o: Eigen/src/QR/ColPivHouseholderQR.h
Tools.o: Eigen/src/QR/CompleteOrthogonalDecomposition.h
Tools.o: Eigen/src/misc/RealSvd2x2.h Eigen/src/SVD/UpperBidiagonalization.h
Tools.o: Eigen/src/SVD/SVDBase.h Eigen/src/SVD/JacobiSVD.h
Tools.o: Eigen/src/SVD/BDCSVD.h Eigen/src/Geometry/OrthoMethods.h
Tools.o: Eigen/src/Geometry/EulerAngles.h Eigen/src/Geometry/Homogeneous.h
Tools.o: Eigen/src/Geometry/RotationBase.h Eigen/src/Geometry/Rotation2D.h
Tools.o: Eigen/src/Geometry/Quaternion.h Eigen/src/Geometry/AngleAxis.h
Tools.o: Eigen/src/Geometry/Transform.h Eigen/src/Geometry/Translation.h
Tools.o: Eigen/src/Geometry/Scaling.h Eigen/src/Geometry/Hyperplane.h
Tools.o: Eigen/src/Geometry/ParametrizedLine.h
Tools.o: Eigen/src/Geometry/AlignedBox.h Eigen/src/Geometry/Umeyama.h
Tools.o: Eigen/src/Eigenvalues/Tridiagonalization.h
Tools.o: Eigen/src/Eigenvalues/RealSchur.h
Tools.o: Eigen/src/Eigenvalues/./HessenbergDecomposition.h
Tools.o: Eigen/src/Eigenvalues/EigenSolver.h
Tools.o: Eigen/src/Eigenvalues/./RealSchur.h
Tools.o: Eigen/src/Eigenvalues/SelfAdjointEigenSolver.h
Tools.o: Eigen/src/Eigenvalues/./Tridiagonalization.h
Tools.o: Eigen/src/Eigenvalues/GeneralizedSelfAdjointEigenSolver.h
Tools.o: Eigen/src/Eigenvalues/HessenbergDecomposition.h
Tools.o: Eigen/src/Eigenvalues/ComplexSchur.h
Tools.o: Eigen/src/Eigenvalues/ComplexEigenSolver.h
Tools.o: Eigen/src/Eigenvalues/./ComplexSchur.h
Tools.o: Eigen/src/Eigenvalues/RealQZ.h
Tools.o: Eigen/src/Eigenvalues/GeneralizedEigenSolver.h
Tools.o: Eigen/src/Eigenvalues/./RealQZ.h
Tools.o: Eigen/src/Eigenvalues/MatrixBaseEigenvalues.h
