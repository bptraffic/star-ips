// -*- c++ -*-
/**
 * This file is part of:
 *    star-ips -- a family of incremental GMRF estimation algorithms.
 *
 * Authors:
 *   Cyril Furtlehner, cyril.furtlehner@inria.fr
 *   Victorin Martin, victorin.martin@gmail.com
 *   Jean-Marc Lasgouttes, jean-marc.lasgouttes@inria.fr
 *
 * This software is distributed under the Creative Commons License
 * BY-NC-SA. The complete license can be found here
 *   http://creativecommons.org/licenses/by-nc-sa/4.0/legalcode
 */

#ifndef IPSMETHOD_H
#define IPSMETHOD_H

#include "Tools.h"

#include "Eigen/Core"

using namespace std;

class GMRF;

extern Eigen::MatrixXd A, IA, C;

extern GMRF *TheModel;
extern vector2 <double> wLL;	///LL weights


double get_LL (Eigen::MatrixXd const &, Eigen::MatrixXd const &);
double add_maxLL_link (bool);
double get_LL_bound ();
double block_update (int);
void pre_select_links(double);

void init_matrices ();

#endif //IPSMETHOD_H
